<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
        Commands\AfterDeploy::class,
        Commands\CreateModel::class,
        Commands\CreateController::class,
        Commands\Crawlers\Istanbul::class,
        Commands\Crawlers\Antalya::class,
        Commands\Crawlers\AntalyaKumluca::class,
        Commands\Crawlers\Izmir::class,
        Commands\Crawlers\Mersin::class,
        Commands\Crawlers\Karaculha::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->command('crawl:istanbul')->dailyAt('10:00');

    }
}
