<?php

namespace App\Console\Commands;


class CreateModel extends BaseCommands
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cosku:model';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates Model and Migration for Laravel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = ucfirst($this->ask('What is your model name?'));
        $this->info('Okay');

        $command = 'php artisan make:model Models/'.$name;

        $migration = $this->ask('So, do you prefer to create migration? y/n');

        if($migration === 'y'){
            $command .= ' --migration';
        }

        $this->runExec($command);

        $seeder = $this->ask('Seeder? y/n');

        if($seeder === 'y'){
            $this->runExec('php artisan make:seeder '.$name.'sTableSeeder');
        }





    }
}
