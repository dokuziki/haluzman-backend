<?php

namespace App\Console\Commands\Crawlers;


use App\Models\Price;
use Carbon\Carbon;
use QueryPath\QueryPath;


class Istanbul extends Crawling
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:istanbul';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawl all related data for the istanbul';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $url = 'http://www.ibb.gov.tr/sites/haller/tr-TR/Pages/halfiyatlari.aspx';
        parent::__construct($url);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info('Crawling: '.$this->url);
        $dom = $this->init();

        $table = qp($dom)->first('#fiyatlar_tbl')->first('tbody');

        $datas = [];
        foreach($table->find('tr') as $element){

            $tds = $element->find('td')->toArray();
            if(array_key_exists(0,$tds)){
                $datas[] = [
                    'name' => qp(qp($tds[0])->find('span')->toArray()[0])->text(),
                    'min' => str_replace([' TL',','], ['','.'],qp($tds[2])->text()),
                    'max' => str_replace([' TL',','], ['','.'],qp($tds[3])->text())
                ];
            }
        }

        foreach($datas as $data){
            if(isset($this->productMap[1][trim($data['name'])])){
                $target = $this->productMap[1][trim($data['name'])];
                $this->info($data['name'].' Eklendi');

                Price::create([
                    'product_id' => $target,
                    'market_id' => 1,
                    'min' => $data['min'],
                    'max' => $data['max'],
                    'date' => Carbon::now()->format('Y-m-d')
                ]);

                Price::create([
                    'product_id' => $target,
                    'market_id' => 2,
                    'min' => $data['min'],
                    'max' => $data['max'],
                    'date' => Carbon::now()->format('Y-m-d')
                ]);
            }
        }

    }
}
