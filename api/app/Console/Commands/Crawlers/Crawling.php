<?php
/**
 * Created by PhpStorm.
 * User: coskudemirhan
 * Date: 10/06/16
 * Time: 11:46
 */

namespace App\Console\Commands\Crawlers;


use App\Console\Commands\BaseCommands;
use App\Models\MarketProduct;
use Masterminds\HTML5;
use QueryPath\QueryPath;

abstract class Crawling extends BaseCommands
{
    protected $url;
    protected $productMap;

    public function __construct($url)
    {
        $this->url = $url;

            /*

            ['group_id' => 1, 'title' => 'Domates'], 1
            ['group_id' => 1, 'title' => 'Domates LÜX (İhracat)'], 2
            ['group_id' => 1, 'title' => 'Yeşil Domates'], 3
            ['group_id' => 1, 'title' => 'Kokteyl Domates'], 4
            ['group_id' => 1, 'title' => 'Kokteyl Oval'], 5
            ['group_id' => 1, 'title' => 'Kokteyl Salkım'], 6
            ['group_id' => 1, 'title' => 'Kokteyl Yuvarlak'], 7
            ['group_id' => 1, 'title' => 'Cherry Domates'], 8
            ['group_id' => 1, 'title' => 'Salkım'], 9
            ['group_id' => 1, 'title' => 'Beef'], 10


            ['group_id' => 2, 'title' => 'Çarliston'], 11
            ['group_id' => 2, 'title' => 'Sivri Biber'], 12
            ['group_id' => 2, 'title' => 'İnce Sivri'], 13
            ['group_id' => 2, 'title' => 'Kalın Sivri'], 14
            ['group_id' => 2, 'title' => 'Dolma Biber'], 15
            ['group_id' => 2, 'title' => 'Kapya'], 16
            ['group_id' => 2, 'title' => 'Köy Biberi'], 17
            ['group_id' => 2, 'title' => 'Kıl Biber'], 18
            ['group_id' => 2, 'title' => 'Kaliforniya'], 19
            ['group_id' => 2, 'title' => 'Kaliforniya Sarı'], 20
            ['group_id' => 2, 'title' => 'Kaliforniya Kırmızı'], 21

            ['group_id' => 3, 'title' => 'Hıyar'], 22
            ['group_id' => 3, 'title' => 'Silor'], 23
            ['group_id' => 3, 'title' => 'Salata Çengelköy Silor'], 24
            ['group_id' => 3, 'title' => 'Silor Paket'], 25
            ['group_id' => 3, 'title' => 'Silor Badem'], 26
            ['group_id' => 3, 'title' => 'Dikenli'], 27

            ['group_id' => 4, 'title' => 'Iceberg'], 28
            ['group_id' => 4, 'title' => 'Kıvırcık'], 29
            ['group_id' => 4, 'title' => 'Yedikule'], 30
            ['group_id' => 4, 'title' => 'Lollorosso'], 31

            ['group_id' => 5, 'title' => 'Patlıcan'], 32
            ['group_id' => 5, 'title' => 'Bostan'], 33
            ['group_id' => 5, 'title' => 'Kristal'], 34
            ['group_id' => 5, 'title' => 'Tünel'], 35
            ['group_id' => 5, 'title' => 'Uzun'], 36
            ['group_id' => 5, 'title' => 'Topan'], 37
            ['group_id' => 5, 'title' => 'Tophane'], 38
            ['group_id' => 5, 'title' => 'Anamur'], 39
            ['group_id' => 5, 'title' => 'Topak'], 40

            ['group_id' => 5, 'title' => 'Karpuz'], 41

            */

        $marketProducts = MarketProduct::all();

        $this->productMap = [];

        foreach($marketProducts as $marketProduct){
            $this->productMap[$marketProduct->market_id][$marketProduct->crawl_name] =  $marketProduct->product_id;
        }

        parent::__construct($url);
    }

    public function init(){

        $string = file_get_contents($this->url);

        $html5 = new HTML5();
        return $html5->loadHTML($string);
    }
}