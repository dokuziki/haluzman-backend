<?php

namespace App\Console\Commands\Crawlers;


use App\Models\Price;
use Carbon\Carbon;
use QueryPath\QueryPath;


class Karaculha extends Crawling
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:karaculha';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawl all related data for the karaçulha';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $url = 'http://halfiyatlari.net/fethiye-hal-fiyatlari';
        parent::__construct($url);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info('Crawling: '.$this->url);
        $dom = $this->init();

        $table = qp($dom)->first('table');

        $datas = [];
        foreach($table->find('tr') as $element){

            $tds = $element->find('td')->toArray();

            if(count($tds) >2){
                $fiyat = explode('-', trim(preg_replace('/\s+/', ' ', str_replace([' TL',','], ['','.'],qp($tds[1])->text()))));

                $datas[] = [
                    'name' => trim(qp($tds[0])->text()),
                    'min' => trim($fiyat[0]),
                    'max' => trim(@$fiyat[1])
                ];
            }

        }


        foreach($datas as $data){
            if(isset($this->productMap[7][trim($data['name'])])){
                $target = $this->productMap[7][trim($data['name'])];
                $this->info($data['name'].' Eklendi');

                Price::create([
                    'product_id' => $target,
                    'market_id' => 7,
                    'min' => $data['min'],
                    'max' => $data['max'],
                    'date' => Carbon::now()->format('Y-m-d')
                ]);

            }
        }

    }
}
