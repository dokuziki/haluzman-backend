<?php

namespace App\Console\Commands\Crawlers;


use App\Models\Price;
use Carbon\Carbon;
use QueryPath\QueryPath;


class Antalya extends Crawling
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:antalya';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawl all related data for the antalya merkes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $url = 'http://halfiyatlari.net/antalya-hal-fiyatlari';
        parent::__construct($url);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info('Crawling: '.$this->url);
        $dom = $this->init();

        $table = qp($dom)->first('.table-responsive');

        $datas = [];
        foreach($table->find('tr') as $element){

            $tds = $element->find('td')->toArray();

            $datas[] = [
                'name' => trim(preg_replace('/\s+/', ' ', str_replace([' TL',','], ['','.'],qp($tds[0])->text()))),
                'min' => trim(preg_replace('/\s+/', ' ', str_replace([' TL',','], ['','.'],qp($tds[1])->text()))),
                'max' => trim(preg_replace('/\s+/', ' ', str_replace([' TL',','], ['','.'],qp($tds[2])->text())))
            ];

        }


        foreach($datas as $data){
            if(isset($this->productMap[3][trim($data['name'])])){
                $target = $this->productMap[3][trim($data['name'])];
                $this->info($data['name'].' Eklendi');

                Price::create([
                    'product_id' => $target,
                    'market_id' => 3,
                    'min' => $data['min'],
                    'max' => $data['max'],
                    'date' => Carbon::now()->format('Y-m-d')
                ]);

            }
        }

    }
}
