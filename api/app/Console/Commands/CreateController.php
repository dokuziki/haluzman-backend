<?php

namespace App\Console\Commands;


class CreateController extends BaseCommands
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cosku:controller';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates Controller  for Laravel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = ucfirst($this->ask('What is your controller name?'));
        $this->info('Okay');

        $namespace = ucfirst($this->ask('Any namespace?'));


        $command = 'php artisan make:controller '.trim($namespace).'/'.trim($name).'Controller ';

        $migration = $this->ask('Is this plain? y/n');

        if($migration === 'y'){
            $command .= ' --plain';
        }

        $this->runExec($command);
    }
}
