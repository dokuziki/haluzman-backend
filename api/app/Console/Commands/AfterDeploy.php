<?php

namespace App\Console\Commands;



class AfterDeploy extends BaseCommands
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cosku:deploy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'After Deploy Command for Laravel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->runExec('php artisan down');
        $this->runExec('php artisan route:clear');

        $this->runExec('composer dump-autoload');

        $seed = $this->ask('Seeder gonna be execute? y/n');

        if('n' !== $seed && 'y' === $seed){
            $this->runExec('php  artisan migrate:refresh --seed');
        }else{
            $this->runExec('php  artisan migrate:refresh');
        }

        //$this->runExec('php artisan route:cache');
        $this->runExec('php artisan up');
    }
}
