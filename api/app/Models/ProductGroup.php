<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductGroup extends Model
{
    public function getImageAttribute($value)
    {
        return asset('uploads/products/'.$value);
    }

    public function getDetailImageAttribute($value)
    {
        return asset('uploads/products/'.$value);
    }

    public function products($marketid){
        $products = $this->hasMany('App\Models\Product','group_id')->get();
        $response = [];
        foreach($products as $product){
            $response[] = array_merge($product->toArray(), ['price' => $product->prices($marketid)]);
        }

        return $response;
    }
}
