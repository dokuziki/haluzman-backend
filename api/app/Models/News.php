<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    //
    public function getThumbAttribute($value)
    {
        return asset('uploads/news/'.$value);
    }

    public function getImgAttribute($value)
    {
        return asset('uploads/news/'.$value);
    }

    public function getVideoAttribute($value)
    {
        if($value === null){
            return '';
        }else{
            return asset('uploads/news/'.$value);
        }
    }

    public function getTypeAttribute($value)
    {
        return (int) $value;
    }
}
