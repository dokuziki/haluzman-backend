<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function getImageAttribute($value)
    {
        return asset('uploads/products/'.$value);
    }

    public function group()
    {
        return $this->belongsTo('App\Models\ProductGroup', 'group_id');
    }

    public function prices($marketid)
    {
        $prices = $this->hasMany('App\Models\Price', 'product_id')->where('market_id',$marketid)->orderBy('id', 'desc')->take(2)->get()->toArray();


        if (isset($prices[0]) && isset($prices[1])) {
            if ($prices[0]['min'] > $prices[1]['min']) {
                $status = 'up';
            } elseif ($prices[0]['min'] < $prices[1]['min']) {
                $status = 'down';
            } else {
                $status = 'same';
            }
            return [
                'min' => number_format($prices[0]['min'], 2, '.',''),
                'max' => number_format($prices[0]['max'], 2, '.',''),
                'status' => $status
            ];
        } else {
            return [
                'min' => '',
                'max' => '',
                'status' => 'same'
            ];
        }


    }

    public function pricesStatisticsForDaily($marketid,$filter=false){

        $prices = $this->hasMany('App\Models\Price', 'product_id')
            ->where('market_id',$marketid)
            ->orderBy('id', 'desc')
            ->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()])
            ->get();

        $prices = collect($prices)->groupBy('date');

        $data = [];

        $last = false;

        $prices = array_reverse($prices->toArray());

        foreach($prices as $key => $price){

            $add = [];

            if($last){

                if($last['min'] < $price[0]['min']){
                    $add['min_status'] = 'up';
                }elseif($last['min'] === $price[0]['min']){
                    $add['min_status'] = 'same';
                }else{
                    $add['min_status'] = 'down';
                }


                if($last['max'] < $price[0]['max']){
                    $add['max_status'] = 'up';
                }elseif($last['max'] === $price[0]['max']){
                    $add['max_status'] = 'same';
                }else{
                    $add['max_status'] = 'down';
                }

            }else{
                $add = [
                    'max_status' => 'same',
                    'min_status' => 'same'
                ];
            }

            $add['date'] = $key;

            $last = [
                'min' => $price[0]['min'],
                'max' => $price[0]['max']
            ];

            unset($price[0]['created_at']);
            unset($price[0]['updated_at']);
            unset($price[0]['id']);
            unset($price[0]['product_id']);
            unset($price[0]['market_id']);


            $data[] = array_merge($price[0], $add);
        }


        return array_reverse($data);

    }

    public function pricesStatisticsForWeekly($marketid,$filter=false){

        $prices = $this->hasMany('App\Models\Price', 'product_id')
            ->where('market_id',$marketid)
            ->orderBy('id', 'desc')
            ->whereBetween('created_at', [Carbon::now()->subDays(70), Carbon::now()])
            ->get();

        $prices = collect($prices)->groupBy('date');


        $data = [];

        $weekly = [];

        $prices = array_reverse($prices->toArray());

        foreach($prices as $key => $price){

            $tDate = new \DateTime($key);
            $tWeek = $tDate->format("W");
            $weekly[$tWeek] = $price;

        }

        $lastW = false;

        foreach($weekly as $key => $week){


            $daysMinSum = 0;
            $daysMaxSum = 0;

            foreach($week as $day){
                $daysMinSum +=(int)$day['min'];
                $daysMaxSum +=(int)$day['max'];

                $date = new \DateTime($day['created_at']);

            }

            $daysMinAvr = $daysMinSum/count($week);
            $daysMaxAvr = $daysMaxSum/count($week);


            $week = $date->format("W");

            $add = [];

            if($lastW){

                if($lastW['min'] < $daysMinAvr){
                    $add['min_status'] = 'up';
                }elseif($lastW['min'] == $daysMinAvr){
                    $add['min_status'] = 'same';
                }else{
                    $add['min_status'] = 'down';
                }


                if($lastW['max'] < $daysMaxAvr){
                    $add['max_status'] = 'up';
                }elseif($lastW['max'] == $daysMaxAvr){
                    $add['max_status'] = 'same';
                }else{
                    $add['max_status'] = 'down';
                }


            }else{
                $add = [
                    'max_status' => 'same',
                    'min_status' => 'same'
                ];
            }


            $lastW = [
                'min' => $daysMinAvr,
                'max' => $daysMaxAvr
            ];

            $add['date'] = $week;


            $data[] = array_merge(['date' => $key, 'min' => (String) $daysMinAvr, 'max' => (String) $daysMaxAvr ], $add);

        }


        return array_reverse($data);

    }



    public function pricesStatisticsForMonthly($marketid,$filter=false){

        $prices = $this->hasMany('App\Models\Price', 'product_id')
            ->where('market_id',$marketid)
            ->orderBy('id', 'desc')
            ->whereBetween('created_at', [Carbon::now()->subDays(360), Carbon::now()])
            ->get();

        $prices = collect($prices)->groupBy('date');

        $data = [];

        $monthly = [];

        $prices = array_reverse($prices->toArray());

        foreach($prices as $key => $price){

            $tDate = new \DateTime($key);
            $tMonth = $tDate->format("n");
            $monthly[$tMonth] = $price;

        }

        $lastW = false;

        foreach($monthly as $key => $month){


            $daysMinSum = 0;
            $daysMaxSum = 0;

            foreach($month as $day){
                $daysMinSum +=(int)$day['min'];
                $daysMaxSum +=(int)$day['max'];

                $date = new \DateTime($day['created_at']);

            }


            $daysMinAvr = $daysMinSum/count($month);
            $daysMaxAvr = $daysMaxSum/count($month);
            $week = $date->format("n");



            $add = [];

            if($lastW){

                if($lastW['min'] < $daysMinAvr){
                    $add['min_status'] = 'up';
                }elseif($lastW['min'] == $daysMinAvr){
                    $add['min_status'] = 'same';
                }else{
                    $add['min_status'] = 'down';
                }


                if($lastW['max'] < $daysMaxAvr){
                    $add['max_status'] = 'up';
                }elseif($lastW['max'] == $daysMaxAvr){
                    $add['max_status'] = 'same';
                }else{
                    $add['max_status'] = 'down';
                }


            }else{
                $add = [
                    'max_status' => 'same',
                    'min_status' => 'same'
                ];
            }


            $lastW = [
                'min' => $daysMinAvr,
                'max' => $daysMaxAvr
            ];

            $add['date'] = (String) $week;


            $data[] = array_merge(['date' => $key, 'min' => (String) $daysMinAvr, 'max' => (String) $daysMaxAvr ], $add);

        }


        return array_reverse($data);

    }


    public function pricesStatisticsFor3Monthly($marketid,$filter=false){

        $prices = $this->hasMany('App\Models\Price', 'product_id')
            ->where('market_id',$marketid)
            ->orderBy('id', 'desc')
            ->whereBetween('created_at', [Carbon::now()->subDays(360), Carbon::now()])
            ->get();

        $prices = collect($prices)->groupBy('date');

        $data = [];

        $monthly = [];

        $prices = array_reverse($prices->toArray());

        $MONTH3 = [
            1 => 1,
            2 => 1,
            3 => 1,

            4 => 2,
            5 => 2,
            6 => 2,

            7 => 3,
            8 => 3,
            9 => 3,

            10 => 4,
            11 => 4,
            12 => 4
        ];

        foreach($prices as $key => $price){

            $tDate = new \DateTime($key);
            $tMonth = $tDate->format("n");
            $monthly[$MONTH3[$tMonth]] = $price;

        }

        $lastW = false;

        foreach($monthly as $key => $month){


            $daysMinSum = 0;
            $daysMaxSum = 0;

            foreach($month as $day){
                $daysMinSum +=(int)$day['min'];
                $daysMaxSum +=(int)$day['max'];

                $date = new \DateTime($day['created_at']);

            }


            $daysMinAvr = $daysMinSum/count($month);
            $daysMaxAvr = $daysMaxSum/count($month);
            $week = $date->format("n");



            $add = [];

            if($lastW){

                if($lastW['min'] < $daysMinAvr){
                    $add['min_status'] = 'up';
                }elseif($lastW['min'] == $daysMinAvr){
                    $add['min_status'] = 'same';
                }else{
                    $add['min_status'] = 'down';
                }


                if($lastW['max'] < $daysMaxAvr){
                    $add['max_status'] = 'up';
                }elseif($lastW['max'] == $daysMaxAvr){
                    $add['max_status'] = 'same';
                }else{
                    $add['max_status'] = 'down';
                }


            }else{
                $add = [
                    'max_status' => 'same',
                    'min_status' => 'same'
                ];
            }


            $lastW = [
                'min' => $daysMinAvr,
                'max' => $daysMaxAvr
            ];

            $add['date'] = (String) $MONTH3[$week];


            $data[] = array_merge(['date' => $key, 'min' => (String) $daysMinAvr, 'max' => (String) $daysMaxAvr ], $add);

        }


        return array_reverse($data);

    }

    public function pricesStatisticsFor6Monthly($marketid,$filter=false){

        $prices = $this->hasMany('App\Models\Price', 'product_id')
            ->where('market_id',$marketid)
            ->orderBy('id', 'desc')
            ->whereBetween('created_at', [Carbon::now()->subDays(360), Carbon::now()])
            ->get();

        $prices = collect($prices)->groupBy('date');

        $data = [];

        $monthly = [];

        $prices = array_reverse($prices->toArray());

        $MONTH3 = [
            1 => 1,
            2 => 1,
            3 => 1,
            4 => 1,
            5 => 1,
            6 => 1,

            7 => 2,
            8 => 2,
            9 => 2,
            10 => 2,
            11 => 2,
            12 => 2
        ];

        foreach($prices as $key => $price){

            $tDate = new \DateTime($key);
            $tMonth = $tDate->format("n");
            $monthly[$MONTH3[$tMonth]] = $price;

        }

        $lastW = false;

        foreach($monthly as $key => $month){


            $daysMinSum = 0;
            $daysMaxSum = 0;

            foreach($month as $day){
                $daysMinSum +=(int)$day['min'];
                $daysMaxSum +=(int)$day['max'];

                $date = new \DateTime($day['created_at']);

            }


            $daysMinAvr = $daysMinSum/count($month);
            $daysMaxAvr = $daysMaxSum/count($month);
            $week = $date->format("n");



            $add = [];

            if($lastW){

                if($lastW['min'] < $daysMinAvr){
                    $add['min_status'] = 'up';
                }elseif($lastW['min'] == $daysMinAvr){
                    $add['min_status'] = 'same';
                }else{
                    $add['min_status'] = 'down';
                }


                if($lastW['max'] < $daysMaxAvr){
                    $add['max_status'] = 'up';
                }elseif($lastW['max'] == $daysMaxAvr){
                    $add['max_status'] = 'same';
                }else{
                    $add['max_status'] = 'down';
                }


            }else{
                $add = [
                    'max_status' => 'same',
                    'min_status' => 'same'
                ];
            }


            $lastW = [
                'min' => $daysMinAvr,
                'max' => $daysMaxAvr
            ];

            $add['date'] = (String) $MONTH3[$week];


            $data[] = array_merge(['date' => $key, 'min' => (String) $daysMinAvr, 'max' => (String) $daysMaxAvr ], $add);

        }


        return array_reverse($data);

    }

    public function pricesStatisticsForYearly($marketid,$filter=false){

        $prices = $this->hasMany('App\Models\Price', 'product_id')
            ->where('market_id',$marketid)
            ->orderBy('id', 'desc')
            ->get();

        $prices = collect($prices)->groupBy('date');

        $data = [];

        $monthly = [];

        $prices = array_reverse($prices->toArray());


        foreach($prices as $key => $price){

            $tDate = new \DateTime($key);
            $tMonth = $tDate->format("Y");
            $monthly[$tMonth] = $price;

        }

        $lastW = false;

        foreach($monthly as $key => $month){


            $daysMinSum = 0;
            $daysMaxSum = 0;

            foreach($month as $day){
                $daysMinSum +=(int)$day['min'];
                $daysMaxSum +=(int)$day['max'];

                $date = new \DateTime($day['created_at']);

            }


            $daysMinAvr = $daysMinSum/count($month);
            $daysMaxAvr = $daysMaxSum/count($month);
            $week = $date->format("Y");



            $add = [];

            if($lastW){

                if($lastW['min'] < $daysMinAvr){
                    $add['min_status'] = 'up';
                }elseif($lastW['min'] == $daysMinAvr){
                    $add['min_status'] = 'same';
                }else{
                    $add['min_status'] = 'down';
                }


                if($lastW['max'] < $daysMaxAvr){
                    $add['max_status'] = 'up';
                }elseif($lastW['max'] == $daysMaxAvr){
                    $add['max_status'] = 'same';
                }else{
                    $add['max_status'] = 'down';
                }


            }else{
                $add = [
                    'max_status' => 'same',
                    'min_status' => 'same'
                ];
            }


            $lastW = [
                'min' => $daysMinAvr,
                'max' => $daysMaxAvr
            ];

            $add['date'] = (String) $week;


            $data[] = array_merge(['date' => $key, 'min' => (String) $daysMinAvr, 'max' => (String) $daysMaxAvr ], $add);

        }


        return array_reverse($data);

    }


    public function markets()
    {
        return $this->belongsToMany('\App\Models\Market', 'market_products');
    }
}
