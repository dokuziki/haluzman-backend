<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    public function getImageAttribute($value)
    {
        return asset('uploads/catalogs/'.$value);
    }

    public function getFileAttribute($value)
    {
        return asset('uploads/file/'.$value);
    }

}
