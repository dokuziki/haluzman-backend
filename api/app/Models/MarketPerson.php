<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarketPerson extends Model
{
    public function getPicAttribute($value)
    {
        return asset('uploads/persons/'.$value);
    }
}
