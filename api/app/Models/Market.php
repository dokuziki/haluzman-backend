<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Market extends Model
{
    //
    protected $hidden = ['created_at','updated_at'];

    public function products()
    {
        return $this->belongsToMany('App\Models\Product','market_products');
    }

    public function productsByGroup($groupid)
    {

        $products = $this->belongsToMany('App\Models\Product','market_products')->where(DB::raw('products.group_id'),$groupid)->get();
        $response = [];
        foreach($products as $product){
            $response[] = array_merge($product->toArray(), ['price' => $product->prices($this->id)]);
        }

        return $response;
    }

    public function groups()
    {
        $products =  $this->products()->get();
        $groupProducts = [];
        foreach($products as $product){
            unset($product['pivot']);
            $groupProducts[$product->group_id][] = $product->toArray();
        }

        $group_ids = array_keys($groupProducts);

        $groups = ProductGroup::whereIn('id', $group_ids)->get();

        $response = [];
        foreach($groups->toArray() as $group){
            $group['products'] = $groupProducts[$group['id']];

            $response[] = $group;
        }
        return $response;
    }

    public function persons(){
        return $this->hasMany('App\Models\MarketPerson','market_id');
    }
}
