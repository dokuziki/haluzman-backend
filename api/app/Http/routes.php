<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Support\Facades\Route;


Route::controller('crawler', 'CrawlerController');

//adminlogin
Route::get('/backoffice/login', 'Auth\AdminController@getLogin');
Route::post('/backoffice/login', 'Auth\AdminController@postLogin');
Route::get('/backoffice/logout', 'Auth\AdminController@getLogout');


Route::group(['prefix' => 'backoffice', 'middleware' => ['authAdmin'], 'namespace' => 'Panel'], function () {

    Route::controller('catalogs', 'CatalogsController',
        [
            'getIndex' => 'admin.catalogs.index',
            'getUpsert' => 'admin.catalogs.upsert',
            'postUpsert' => 'admin.catalogs.upsert'

        ]
    );


    Route::controller('user', 'UserController',
        [
            'getIndex' => 'admin.user.index',
            'getUpsert' => 'admin.user.upsert',
            'postUpsert' => 'admin.user.upsert'

        ]
    );

    Route::controller('product', 'ProductController',
        [
            'getIndex' => 'admin.product.index',
        ]
    );

    Route::controller('markets', 'MarketController',
        [
            'getIndex' => 'admin.markets.index',
        ]
    );


    Route::controller('news', 'NewsController',
        [
           'getIndex' => 'admin.news.index',
           'getNews'      => 'admin.news.news',
           'getUpsert' => 'admin.news.upsert',
            'postUpsert' => 'admin.news.upsert'

        ]
    );
    Route::controller('/', 'HomeController', ['getIndex' => 'admin.dash']);

});

Route::group(['namespace' => 'Api'], function () {

    Route::get('terms','HomeController@getTerms');
    Route::get('privacy','HomeController@getPrivacy');


    Route::group(['middleware' => ['requestValidation','token']], function () {
        /*
            Route::controller('cart', 'CartController');
            Route::controller('payment', 'PaymentController',['middleware' => ['auth']]);
            Route::controller('customer', 'Auth\AuthController');
            Route::controller('product', 'ProductController');
        */


        Route::controller('news', 'NewsController');
        Route::controller('user', 'UserController');
        Route::controller('device', 'DeviceController');
        Route::controller('market', 'MarketController');
        Route::controller('/', 'HomeController');
    });

});


