<?php
/**
 * Created by PhpStorm.
 * User: coskudemirhan
 * Date: 09/06/16
 * Time: 15:32
 */


namespace App\Http\Responses;

class NotFound extends BaseResponse
{

    private static $instance = null;

    function __construct()
    {
        parent::setHeader('404');
        parent::setStatus(false);
        parent::setError('');
        parent::setData('');
    }


    public static function message($message)
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        self::$instance->setMessages($message);


        return self::$instance;
    }

    public function error($error){
        parent::setError($error);
        return $this;
    }


    public function send(){
        if(parent::getMessages() == null)
            parent::setMessages("");

        return response()->json([
            'status' => parent::getStatus(),
            'error' => parent::getError(),
            'messages' => parent::getMessages(),
            'data' => parent::getData()
        ], parent::getHeader());
    }
}