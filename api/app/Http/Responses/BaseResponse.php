<?php
/**
 * Created by PhpStorm.
 * User: coskudemirhan
 * Date: 14/02/16
 * Time: 18:26
 */

namespace App\Http\Responses;

class BaseResponse
{
    /**
     * @return mixed
     */
    public static function getHeader()
    {
        return self::$header;
    }

    /**
     * @param mixed $header
     */
    public static function setHeader($header)
    {
        self::$header = $header;
    }

    /**
     * @return mixed
     */
    public static function getStatus()
    {
        return self::$status;
    }

    /**
     * @param mixed $status
     */
    public static function setStatus($status)
    {
        self::$status = $status;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param mixed $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * @return mixed
     */
    public static function getData()
    {
        return self::$data;
    }

    /**
     * @param mixed $data
     */
    public static function setData($data)
    {
        self::$data = $data;
    }


    public static $header;
    public static $status;
    public $error;
    public $messages;
    public static $data;

}