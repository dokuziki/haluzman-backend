<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;

class Token
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->token == null){
            $request->token = Request::header('token');
            Input::merge(array('token' => Request::header('token')));
        }

        if($request->encr == null){
            $request->encr = Request::header('encr');
            Input::merge(array('encr' => Request::header('token')));
        }



        if (!(file_exists(public_path(substr($request->getPathInfo(),1))) && strpos($request->getPathInfo(), '.') )&& isset($request->token) && $request->token != "") {
            $key = env('HASH_KEY');

            try{
                $decoded = JWT::decode(Input::get('token'), $key, ['HS256']);

                if($request->auth !== null){
                    $user = User::where('remember_token',$request->auth)->first();

                    if($user !== null){
                        Auth::loginUsingId($user->id);
                    }else{
                        return $this->error($request);
                    }
                }

                $GLOBALS['client'] = $decoded;

            }catch(\Exception $err){
                //response  errör



            }

            //validation

            return $next($request);

        }
        else if (($request->getPathInfo() == '/device/register'  && $request->getMethod() == 'POST')  ||  $request->getPathInfo() == '/splash') {

            return $next($request);

        } else {

            return $this->error($request);
        }

    }

    private function error($request)
    {
        return response()->json([
            'status' => false,
            'error' => config('messages.error.missedToken'),
            'message' => [ $request->getPathInfo() ],
            'data' => []
        ], 401);
    }

}
