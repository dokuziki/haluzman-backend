<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Config;

class UserController extends Controller
{
    public function getIndex()
    {

        $data['users'] = User::all();

        return view('admin.user.index', $data);

    }

    public function getUpsert($id=false) {

        $data['item'] = User::find($id);
        return view('admin.user.upsert',$data);

    }

    public function postUpsert() {

        $input = Input::all();


        $user = new User();

        if(Input::get('password') != null || Input::get('password') != "")
            $user->password = Hash::make(Input::get('password'));

        $user->exists = (Input::get('id') === "false") ? false : true;



        if ($user->exists) {
            $user->id = Input::get('id');
        }


        $user->first_name = $input['first_name'];
        $user->last_name = $input['last_name'];
        $user->email = $input['email'];
        $user->status = $input['status'];
        $user->admin = $input['role'];

        if($user->save()){
            Session::flash('alert', Config::get('messages.admin.editUserTrue'));
            return redirect()->action('Panel\UserController@getIndex');

        }else{
            Session::flash('error', Config::get('messages.admin.editUserFalse'));
            return redirect()->action('Panel\UserController@getIndex');
        }
    }

    public function getDelete($id){

            $user = new User();
            $user = User::where('id',$id)->first();
            $user->status = 0;

            $user->save();

            Session::flash('alert', 'Kullanıcı İnaktif Hale Getirildi.');
            return Redirect::back();

    }

}
