<?php

namespace App\Http\Controllers\Panel;

use App\Http\Responses\Fail;
use App\Http\Responses\Success;
use App\Models\Market;
use App\Models\MarketProduct;
use App\Models\Price;
use App\Models\Product;
use App\Models\ProductGroup;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    public function getIndex() {

        $data['items'] = ProductGroup::all();
        $data['isGroup']  = true;

        return view('admin.product.index',$data);
    }

    public function getDetail($product_group=false) {

        $data['items'] = Product::where('group_id',$product_group )->get();
        $data['isGroup'] = false;

        return view('admin.product.index',$data);
    }

    public function getUpsert($id=false) {

            if (!$id) {
                $data['item'] = new ProductGroup();
            } else {
                $data['item'] = ProductGroup::find($id);
            }

            return view('admin.product.upsert', $data);
    }

    public function postUpsert() {

        $input = Input::all();
        $product_group = new ProductGroup();
        $product_group->exists = (Input::get('id') === "false") ? false : true;


        if ($product_group->exists) {
            $product_group->id = Input::get('id');
        }
        $product_group->timestamps = false;
        $product_group->title = $input['title'];

        if(isset($input['image'])){

            $name = str_random(15).'.jpg';
            $filename = 'uploads/products/'.$name;
            $file = public_path($filename);

            if(Image::make(Input::file('image'))->save($file)){
                $product_group->image = $name;

            }else{
                Session::flash('error', 'Ürün Grubu Görseli Yüklenirken Bir Sorun Oluştu');
                return redirect()->action('Panel\ProductController@getIndex');
            }
        }

        if($product_group->save()){
            Session::flash('alert', 'Kayıt Edildi.');
            return redirect()->action('Panel\ProductController@getIndex');

        }else{
            Session::flash('error', 'Hata');
            return redirect()->action('Panel\ProductController@getIndex');
        }
    }

    public function getProduct($id=false) {

        if (!$id) {
            $data['item'] = new Product();
            $data['lists'] = new Market();
        } else {
            $data['item'] = Product::find($id);
            $data['lists'] = $data['item']->markets()->get();
        }


        return view('admin.product.product', $data);
    }

    public function postProduct() {

        $input = Input::all();
        $product = new Product();
        $product->exists = (Input::get('id') === "false") ? false : true;


        if ($product->exists) {
            $product->id = Input::get('id');
        }
        $product->timestamps = false;
        $product->title = $input['title'];

        if(isset($input['image'])){

            $name = str_random(15).'.jpg';
            $filename = 'uploads/products/'.$name;
            $file = public_path($filename);

            if(Image::make(Input::file('image'))->save($file)){
                $product->image = $name;

            }else{
                Session::flash('error', 'Ürün Grubu Görseli Yüklenirken Bir Sorun Oluştu');
                return redirect()->action('backoffice/product/product/'.$product->id);
            }
        }

        if($product->save()){
            Session::flash('alert', 'Kayıt Edildi.');
            return redirect('backoffice/product/product/'.$product->id);

        }else{
            Session::flash('error', 'Hata');
            return redirect()->action('Panel\ProductController@getDetail');
        }
    }

    public function getPrice($product_id,$market_id) {

        $data['market'] = Market::find($market_id);
        $data['product'] = Product::find($product_id);

        $data['items'] = Price::where('product_id',$product_id)->where('market_id',$market_id)->get();

        $data['dateBy'] = [];

        foreach($data['items'] as $item){
            $data['dateBy'][$item->date] = $item;
        }

        $data['dates'] = [];
        $now = Carbon::now()->addDay();


        for($i = 0; $i <30; ++$i){
            $data['dates'][] = $now->subDay()->format('Y-m-d');
        }

        return view('admin.product.price', $data);
    }

    public function postPrice(){
        $input = Input::all();

        $price =   Price::where('date', $input['date'])
                        ->where('product_id',$input['product_id'])
                        ->where('market_id',$input['market_id'])
                        ->first();


        if($price !== null){
            if(isset($input['min'])){
                $price->min = $input['min'];
            }

            if(isset($input['max'])){
                $price->max = $input['max'];
            }



        }else{
            $price = new Price();

            $price->date = $input['date'];
            $price->product_id = $input['product_id'];
            $price->market_id = $input['market_id'];

            if(isset($input['min'])){
             $price->min = $input['min'];
            }
            if(isset($input['max'])){
            $price->max = $input['max'];
            }

        }


        if($price->save()){
            return ['status' => true];
        }else{
            return ['status' => false];
        }

    }


}
