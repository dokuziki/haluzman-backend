<?php

namespace App\Http\Controllers\Panel;

use App\Models\Market;
use App\Models\MarketPerson;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Redirect;


class MarketController extends Controller
{
    public function getIndex() {

        $data['items'] = Market::all();
        $data['isGroup']  = true;

        return view('admin.markets.index',$data);
    }

    public function getDetail($market_id=false) {

        $data['items'] = MarketPerson::where('market_id',$market_id)->get();
        $data['isGroup'] = false;
        $data['market_id'] = $market_id;

        return view('admin.markets.index',$data);
    }

    public function getPerson($market_id,$id=false) {

        if (!$id) {
            $data['item'] = new MarketPerson();
            $data['lists'] = new Market();
            $data['market_id'] = $market_id;
        } else {
            $data['item'] = MarketPerson::find($id);

        }

        return view('admin.markets.person', $data);
    }

    public function postPerson() {

        $input = Input::all();
        $person = new MarketPerson();
        $person->exists = (Input::get('id') === "false") ? false : true;


        if ($person->exists) {
            $person->id = Input::get('id');
        }
        $person->timestamps = false;
        $person->name = $input['name'];
        $person->gsm = $input['phone'];
        $person->market_id = $input['market_id'];

        if(isset($input['image'])){

            $name = str_random(15).'.jpg';
            $filename = 'uploads/persons/'.$name;
            $file = public_path($filename);

            if(Image::make(Input::file('image'))->save($file)){
                $person->pic = $name;

            }else{
                Session::flash('error', 'Yetkili Görseli Yüklenirken Bir Sorun Oluştu');
                return redirect()->action('backoffice/markets/person/'.$person->id);
            }
        }

        if($person->save()){
            Session::flash('alert', 'Kayıt Edildi.');
            return redirect('backoffice/markets/detail/'.$person->market_id);

        }else{
            Session::flash('error', 'Hata');
            return redirect()->action('Panel\MarketController@getDetail');
        }

    }

    public function getUpsert($id=false) {

        if (!$id) {
            $data['item'] = new Market();
        } else {
            $data['item'] = Market::find($id);
        }

        return view('admin.markets.upsert', $data);
    }

    public function postUpsert() {

        $input = Input::all();
        $market = Market::where('id', $input['id'])->firstOrFail();

        $market->title = $input['title'];
        $market->location = $input['location'];
        $market->phone = $input['phone'];
        $market->address = $input['address'];

        if($market->save()){
            Session::flash('alert', 'Kayıt Güncellendi.');
            return redirect()->action('Panel\MarketController@getIndex');

        }else{
            Session::flash('error', 'Güncellemede Bir Hata Oluştu');
            return redirect()->action('Panel\MarketController@getIndex');
        }

    }

    public function getDelete($id) {
        $new = MarketPerson::where('id', $id)->first();
        $new->delete();

        Session::flash('alert', 'Haber Başarıyla Silindi');
        return Redirect::back();
    }
}
