<?php

namespace App\Http\Controllers\Panel;

use App\Library\Push\PushHelper;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;

class NewsController extends Controller
{
    protected $count = 15;
    protected $validExts = ['mp4','mov','3gp','m4v'];

    public function getIndex()
    {

        $data['news'] = News::all();

        return view('admin.news.index',$data);

    }


    public function getNews(Request $request) {

        if(Input::get('type') < 9 && Input::get('type') > 0){
            $type = Input::get('type');
        }else{
            $type = 1;
        }

        $data['news'] =  News::orderBy('id', 'DESC')->where('type',$type)->paginate($this->count)->toArray()['data'];

        $typeNames = [
                1 => "Halden Haberler",
                2 => "Uzman Bakışı - Haldekiler",
                3 => "Uzman Bakışı - Nunhems Uzmanları",
                4 => "Dünya Halleri - Dünyadan Haberler",
                5 => "Dünya Halleri - Galeri",
                6 => "Dünya Halleri - Manşet",
                7 => "Uzman Bakışı - Manşet",
                8 => "Halden Haberler - Manşet"
            ];

        $data['title'] = $typeNames[$type];
        $data['type'] = (int) $type;

        return view('admin.news.news',$data);

    }


    public function getUpsert($id=false) {

        if(!$id){
            $data['item'] = new News();
        }else{
            $data['item'] = News::find($id);
        }

        return view('admin.news.upsert',$data);

    }

    public function getPush($id){

        $news = News::find($id);

        if($news===null){
            Session::flash('error', 'Haber Bulunamadı.');
            return redirect()->action('Panel\NewsController@getIndex');
        }else{

            $push = PushHelper::sendNewsPush($news);
            if($push){
                $news->push = 1;
                $news->save();
                Session::flash('alert', 'Bildirim Başarıyla Gönderildi.');
                return redirect()->action('Panel\NewsController@getIndex');
            }else{
                Session::flash('error', 'Bir Sorun Oluştu.');
                return redirect()->action('Panel\NewsController@getIndex');
            }
        }
    }

    public function postUpsert() {

        $input = Input::all();
        $news = new News();

        $news->exists = (Input::get('id') === "false") ? false : true;


        if ($news->exists) {
            $news->id = Input::get('id');
        }

        $news->title = $input['title'];
        $news->description = $input['description'];
        $news->source = $input['source'];
        $news->text = $input['content'];
        $news->type = $input['category'];


        if(isset($input['thumb'])){
            $name = str_random(15).'.jpg';
            $filename = 'uploads/news/'.$name;
            $file = public_path($filename);

            if(Image::make(Input::file('thumb'))->save($file)){
                $news->thumb = $name;
            }else{
                Session::flash('error', 'Haber Görseli Yüklenirken Bir Sorun Oluştu');
                return redirect()->action('Panel\NewsController@getIndex');
            }
        }


        if(isset($input['img'])){

            $name = str_random(15).'.jpg';
            $filename = 'uploads/news/'.$name;
            $file = public_path($filename);

            if(Image::make(Input::file('img'))->save($file)){
                $news->img = $name;
            }else{
                Session::flash('error', 'Haber Görseli Yüklenirken Bir Sorun Oluştu');
                return redirect()->action('Panel\NewsController@getIndex');
            }
        }


        if(isset($input['video'])){
            $videoInput = Input::file('video');

            $ext = $videoInput->guessClientExtension();

            if(in_array($ext, $this->validExts)) {

                $filename = str_random(15).'.'.$ext;
                $file = public_path('uploads/news/');

                $videoInput->move($file,$filename);
                $news->video = $filename;

            }else{
                Session::flash('error', 'Beklenmeyen Video Uzantısı');
                return redirect()->action('Panel\NewsController@getIndex');
            }

        }



        if($news->save()){
            Session::flash('alert', 'Kayıt Edildi.');
            return redirect()->action('Panel\NewsController@getIndex');

        }else{
            Session::flash('error', 'Hata');
            return redirect()->action('Panel\NewsController@getIndex');
        }
    }

    public function getDelete($id) {
        $new = News::where('id', $id)->first();
        $new->delete();

        Session::flash('alert', 'Haber Başarıyla Silindi');
        return Redirect::back();
    }
}
