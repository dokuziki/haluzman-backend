<?php

namespace App\Http\Controllers\Panel;

use App\Models\Catalog;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class CatalogsController extends Controller
{

    public function getIndex()
    {
        $data['items'] = Catalog::all();
        return view('admin.catalogs.index', $data);
    }

    public function getUpsert($id = false)
    {

        if (!$id) {
            $data['item'] = new Catalog();
        } else {
            $data['item'] = Catalog::find($id);
        }

        return view('admin.catalogs.upsert', $data);
    }

    public function postUpsert()
    {

        $input = Input::all();
        $catalog = new Catalog();
        $catalog->exists = (Input::get('id') === "false") ? false : true;


        if ($catalog->exists) {
            $catalog->id = Input::get('id');
        }
        $catalog->timestamps = false;
        $catalog->title = $input['title'];
        $catalog->approved = $input['approved'];

        if (isset($input['image'])) {

            $name = str_random(15) . '.jpg';
            $filename = 'uploads/catalogs/' . $name;
            $file = public_path($filename);

            if (Image::make(Input::file('image'))->save($file)) {
                $catalog->image = $name;

            } else {
                Session::flash('error', 'Ürün Grubu Görseli Yüklenirken Bir Sorun Oluştu');
                return redirect()->action('Panel\CatalogsController@getIndex');
            }

        }


        if (isset($input['filepdf'])) {
            $pdfinput = Input::file('filepdf');

                $filename = str_random(15) . '.' . 'pdf';
                $file = public_path('uploads/file/');

                $pdfinput->move($file, $filename);
                $catalog->file = $filename;


           /* if (isset($input['filepdf'])) {

                $file = array_get($input, 'filepdf');
                $destinationPath = 'uploads/file/';
                $filename = str_random(15) . '.pdf';
                $upload_success = $file->move($destinationPath, $filename);

                if ($upload_success) {
                    $catalog->file = $upload_success;

                } else {
                    Session::flash('error', 'PDF Yüklenirken Bir Sorun Oluştu');
                    return redirect()->action('Panel\CatalogsController@getIndex');
                }

            } */

            if ($catalog->save()) {
                Session::flash('alert', 'Kayıt Edildi.');
                return redirect()->action('Panel\CatalogsController@getIndex');

            } else {
                Session::flash('error', 'Hata');
                return redirect()->action('Panel\CatalogsController@getIndex');
            }
        }
    }

        public function getDelete($id = false)
        {

            $catalog = new Catalog();
            $catalog = Catalog::where('id', $id)->first();
            $catalog->approved = 0;

            $catalog->save();
            Session::flash('alert', 'Katalog İnaktif Hale Getirildi.');
            return Redirect::back();
        }

}
