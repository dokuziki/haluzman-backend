<?php

namespace App\Http\Controllers\Api;

use App\Http\Responses\NotFound;
use App\Http\Responses\Success;
use App\Models\News;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;

class NewsController extends Controller
{
    protected $count = 15;

    public function postIndex(){

        $page =  Input::get('page') !== null ? Input::get('page') : 0;

        if($page !== 0)
            --$page;

        $key = urldecode(Input::get('key'));



         $data['result'] = News::where('title', 'LIKE', '%'.$key.'%')->orWhere('description', 'LIKE', '%'.$key.'%')->take($this->count)->skip($page * $this->count)->get();

        return Success::fill($data)->send();
    }

    public function getHalden(){
        $data['news'] =  News::orderBy('id', 'DESC')->where('type',1)->paginate($this->count)->toArray()['data'];

        return Success::fill($data)->send();
    }

    public function getHaldekiler(){
        $data['news'] =  News::orderBy('id', 'DESC')->where('type',2)->paginate($this->count)->toArray()['data'];

        return Success::fill($data)->send();
    }

    public function getUzman(){
        $data['news'] =  News::orderBy('id', 'DESC')->where('type',3)->paginate($this->count)->toArray()['data'];

        return Success::fill($data)->send();
    }


    public function getDunyadan(){
        $data['news'] =  News::orderBy('id', 'DESC')->where('type',4)->paginate($this->count)->toArray()['data'];

        return Success::fill($data)->send();
    }


    public function getGaleri(){
        $data['news'] =  News::orderBy('id', 'DESC')->where('type',5)->paginate($this->count)->toArray()['data'];

        return Success::fill($data)->send();
    }


    public function getDunyaHalleri(){
        $data['news'] =  News::orderBy('id', 'DESC')->where('type',6)->paginate($this->count)->toArray()['data'];

        return Success::fill($data)->send();
    }

    public function getUzmanBakisi(){
        $data['news'] =  News::orderBy('id', 'DESC')->where('type',7)->paginate($this->count)->toArray()['data'];

        return Success::fill($data)->send();
    }

    public function getHaldenHaberler(){
        $data['news'] =  News::orderBy('id', 'DESC')->where('type',8)->paginate($this->count)->toArray()['data'];

        return Success::fill($data)->send();
    }

    public function getDetail(){
        $data['detail'] =  News::find(Input::get('id'));

        if($data['detail']  === null){
            return NotFound::message('Aradığınız Haber Bulunamadı')->send();
        }

        $related = News::orderBy('id', 'DESC')->where('id', '!=', $data['detail']->id);

        $related->where(function($query) use($data){
            foreach(explode('-',Str::slug($data['detail']->title)) as $title){
                $query = $query->orWhere('title', 'LIKE', '%'.$title.'%');
            }

            return $query;
        });

        $data['related'] = $related->take($this->count)->get();


        return Success::fill($data)->send();
    }
}
