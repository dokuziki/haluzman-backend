<?php

/**
 * Created by PhpStorm.
 * User: coskudemirhan
 * Date: 09/06/16
 * Time: 15:01
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\Bad;
use App\Http\Responses\Success;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Input;

class DeviceController extends Controller
{

    public function postRegister(){

        $validation = $this->inputValidation
            (
                [
                    'language' => 'required',
                    'type' => 'required',
                    'app_version' => 'required'
                ]
            );

        if($validation->fails()){
            return Bad::fill()->error(config('messages.error.missedData'))->message($validation->errors()->all())->send();
        }

        $token = [
            'language' => Input::get('language'),
            'type' => Input::get('type'),
            'app_version' => Input::get('app_version')
        ];

        $response['token'] = JWT::encode($token, env('HASH_KEY'));


        return Success::fill($response)->send();
    }


    public function getInfo(){
        $response['data'] = JWT::decode(Input::get('token'), env('HASH_KEY'), ['HS256']);

        return Success::fill($response)->send();
    }
}