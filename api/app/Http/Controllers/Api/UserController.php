<?php
/**
 * Created by PhpStorm.
 * User: coskudemirhan
 * Date: 29/06/16
 * Time: 13:24
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Responses\Bad;
use App\Http\Responses\Fail;
use App\Http\Responses\NotFound;
use App\Http\Responses\Success;
use App\Library\Push\PushHelper;
use App\Models\User;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Laravel\Socialite\Facades\Socialite;

class UserController extends Controller
{

    public $socialite;

    public function __construct(Socialite $socialite)
    {
        $this->socialite = $socialite::driver('social');
    }

    private function facebookUser($accessToken){
        try{
            return  $this->socialite->userFromToken($accessToken);
        }catch(ClientException $err){
            return false;
        }
    }

    public function postFacebook(){
        $fbUser = $this->facebookUser(Input::get('accessToken'));

        if(!$fbUser){
            return Bad::fill()->error(config('messages.error.invalidFBToken'))->message(config('messages.error.invalidFBToken'))->send();
        }

        if($fbUser->email == null || $fbUser->email == ""){
            return Bad::fill()->error(config('messages.error.emptyEmail'))->message(config('messages.error.fbEmptyEmail'))->send();
        }

        $userCheck = User::where('email', '=', $fbUser->email)->first();

        if ($userCheck == null) {
            $user = new User;
            $user->first_name = $fbUser->user['first_name'];
            $user->last_name = $fbUser->user['last_name'];
            $user->email = $fbUser->email;
            $user->fb_id = $fbUser->id;
            $user->fb_token = $fbUser->token;

            $user->birthday = isset($fbUser->user['birthday']) ? date("d-m-Y", strtotime($fbUser->user['birthday'])) : null;

            if($fbUser->user['gender'] == 'male')
                $user->gender = 0;
            else if($fbUser->user['gender'] == 'female')
                $user->gender = 1;
            else
                $user->gender = 2;




            $path = str_replace('normal','large',$fbUser->avatar);
            $filename = 'uploads/'.str_random(15).'.jpg';
            $file = public_path($filename);

            $user->pic = asset($filename);
            Image::make($path)->resize(300,300)->save($file);

            if ($user->save()) {
                //TODO: usera token bilgisi eklenecek
                Auth::login($user, true);
                $this->updateClient($user->getRememberToken());
                return Success::fill(['user' => $this->transformUser($user), 'auth' => $user->getRememberToken()])->send();
            }
        } else {
            Auth::login($userCheck, true);
            $this->updateClient($userCheck->getRememberToken());
            return Success::fill(['user' => $this->transformUser($userCheck), 'auth' => $userCheck->getRememberToken()])->send();
        }
    }

    public function postLogin()
    {
        $input = Input::all();

        $messages = [
            'required' => Config::get('messages.error.required'),
            'email' => Config::get('messages.error.invalidEmail')
        ];

        $cases = [
            'email' => 'required|email|max:255',
            'password' => 'required|min:6',
        ];

        $validator = Validator::make($input, $cases, $messages);


        if ($validator->fails())
            return Bad::fill()->error(config('messages.error.missedData'))->message($validator->errors()->all())->send();

        $user = User::where('email', $input['email'])->first();

        if ($user === null) {
            return NotFound::message('Kullanıcı Bulunamadı')->send();
        }
        if (Hash::check($input['password'],$user->password ))
        {
            Auth::login($user);
            $this->updateClient(Auth::user()->getRememberToken());
            return Success::fill(['user' => $this->transformUser(Auth::user()), 'auth' => $user->getRememberToken()])->send();
        }
        else
            return Fail::fill()->error(config('messages.error.userLogin'))->message(config('messages.error.userLogin'))->send();

    }

    public function postPushUpdate()
    {
        $input = Input::all();

        $messages = [
            'required' => Config::get('messages.error.required')
        ];

        $cases = [
            'registrationid' => 'required'
        ];

        $validator = Validator::make($input, $cases, $messages);


        if ($validator->fails())
            return Bad::fill()->error(config('messages.error.missedData'))->message($validator->errors()->all())->send();

        $result = PushHelper::addPlayer($input['registrationid']);

        if($result){
            return Success::fill(['push' => ['id' => $input['registrationid'], 'oneginal'=> $result]])->send();
        }else{
            return Fail::fill()->error('registrationid keydedilemedi')->message('registrationid keydedilemedi')->send();
        }
    }

    public function postRegister()
    {
        $input = Input::all();

        $messages = [
            'required' => Config::get('messages.error.required'),
            'unique' => Config::get('messages.error.unique'),
            'email' => Config::get('messages.error.invalidEmail'),
            'min' => Config::get('messages.error.min6Pass')
        ];

        $cases = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
        ];

        $validator = Validator::make($input, $cases, $messages);


        if ($validator->fails())
            return Bad::fill()->error(config('messages.error.missedData'))->message($validator->errors()->all())->send();

        $user = User::create([
            'first_name' => $input['first_name'],
            'last_name' => $input['last_name'],
            'email' => $input['email'],
            'password' => bcrypt($input['password'])
        ]);

        if ($user){
            Auth::login($user, true);
            $this->updateClient($user->getRememberToken());
            return Success::fill(['user' => $this->transformUser($user), 'auth' => $user->getRememberToken()])->send();
        }
        else
            return Fail::fill()->error(config('messages.error.userCreate'))->message(config('messages.error.userCreate'))->send();

    }


    public function transformUser($user){

        if($user->gender == null)
            $user->gender = 2;

        if($user->pic == null)
            $user->pic = "";

        if($user->fb_id == null)
            $user->fb_id = "";

        if($user->phone == null)
            $user->phone = "";


        return [
            'id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'birthday' => $user->birthday == null ? "" : $user->birthday,
            'gender' => $user->gender,
            'pic' => $user->pic,
            'fb_id' => $user->fb_id
        ];
    }

    public function updateClient(){

    }
}
