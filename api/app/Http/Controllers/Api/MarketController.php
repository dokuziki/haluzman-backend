<?php

/**
 * Created by PhpStorm.
 * User: coskudemirhan
 * Date: 09/06/16
 * Time: 15:01
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\NotFound;
use App\Http\Responses\Success;
use App\Models\Market;
use App\Models\Product;
use App\Models\ProductGroup;
use App\Models\Setting;
use Illuminate\Support\Facades\Input;

class MarketController extends Controller
{
    public function getIndex()
    {
        return $this->getList();
    }

    public function getList(){
        $markets = Market::with(['persons'])->get();
        $head =  Setting::where('object','market')->first();

        return Success::fill(['head' => $head ,'markets' =>$markets])->send();
    }

    public function getGet($id=false){

        if($id){
            $market = Market::where('id',$id)->with(['persons'])->first();
            if($market === null){
                return NotFound::message('Aradığınız Hal Bulunamadı')->send();
            }else{
                $market = array_merge($market->toArray(), ['details' => $market->groups()]);

                return Success::fill(['market' => $market])->send();
            }
        }else{
            return $this->getList();
        }
    }


    public function getProduct(){

        $marketid = Input::get('market');
        $product_groupid = Input::get('product_group');



        $market = Market::where('id',$marketid)->first();

        $product_group = ProductGroup::where('id',$product_groupid)->first();

        $head = [
            'id' => $product_group->id,
            'title' => $product_group->title,
            'description' => $product_group->title,
            'image' => $product_group->image,
            'detail_image' => $product_group->detail_image,
        ];

        if($market === null || $product_group === null){
            return NotFound::message('Aradığınız Ürün Bulunamadı')->send();
        }else{
            $product_group = array_merge($product_group->toArray(), ['products' => $market->productsByGroup($product_groupid)]);

            return Success::fill(['product_group' => $product_group, 'head' => $head])->send();
        }

    }


    public function getProductDetail(){

        $marketid = Input::get('market');
        $productid = Input::get('product');


        $market = Market::where('id',$marketid)->first();
        $product = Product::find($productid);

        if($market === null || $product === null){
            return NotFound::message('Aradığınız Ürün Bulunamadı')->send();
        }else{
            $productResult = array_merge($product->toArray(), [
                'pricesDaily' => $product->pricesStatisticsForDaily($marketid),
                'pricesWeekly' => $product->pricesStatisticsForWeekly($marketid),
                'pricesMonthly' => $product->pricesStatisticsForMonthly($marketid),
                'prices3Monthly' => $product->pricesStatisticsFor3Monthly($marketid),
                'prices6Monthly' => $product->pricesStatisticsFor6Monthly($marketid),
                'pricesYearly' => $product->pricesStatisticsForYearly($marketid)
            ]);

            return Success::fill(['product' => $productResult])->send();
        }

    }


    public function getSingleProduct($marketid,$productid){

        $product = Product::find($productid);


        $stats =  $product->pricesStatisticsForDaily($marketid);

        $product->stat = isset($stats[0]) ? $stats[0] : [];

        return $product;
    }

    public function getComparisonProduct(){

        $marketid = Input::get('market');
        $productid1 = Input::get('product1');
        $productid2 = Input::get('product2');


        $market = Market::where('id',$marketid)->first();
        $product1 = Product::find($productid1);
        $product2 = Product::find($productid2);

        if($market === null || $product1 === null|| $product2 === null){
            return NotFound::message('Aradığınız Ürün Bulunamadı')->send();
        }else{
            $productResult = [
                'pricesDaily1' => $product1->pricesStatisticsForDaily($marketid),
                'pricesWeekly1' => $product1->pricesStatisticsForWeekly($marketid),
                'pricesMonthly1' => $product1->pricesStatisticsForMonthly($marketid),
                'prices3Monthly1' => $product1->pricesStatisticsFor3Monthly($marketid),
                'prices6Monthly1' => $product1->pricesStatisticsFor6Monthly($marketid),
                'pricesYearly1' => $product1->pricesStatisticsForYearly($marketid),

                'pricesDaily2' => $product2->pricesStatisticsForDaily($marketid),
                'pricesWeekly2' => $product2->pricesStatisticsForWeekly($marketid),
                'pricesMonthly2' => $product2->pricesStatisticsForMonthly($marketid),
                'prices3Monthly2' => $product2->pricesStatisticsFor3Monthly($marketid),
                'prices6Monthly2' => $product2->pricesStatisticsFor6Monthly($marketid),
                'pricesYearly2' => $product2->pricesStatisticsForYearly($marketid),
            ];

            return Success::fill(['comparison' => $productResult])->send();
        }

    }

    public function getComparisonMarket(){

        $marketid1 = Input::get('market1');
        $marketid2 = Input::get('market2');
        $productid1 = Input::get('product');


        $market1 = Market::where('id',$marketid1)->first();
        $market2 = Market::where('id',$marketid2)->first();
        $product1 = Product::find($productid1);

        if($market1 === null || $product1 === null|| $market2 === null){
            return NotFound::message('Aradığınız Ürün Bulunamadı')->send();
        }else{
            $productResult = [
                'pricesDaily1' => $product1->pricesStatisticsForDaily($marketid1),
                'pricesWeekly1' => $product1->pricesStatisticsForWeekly($marketid1),
                'pricesMonthly1' => $product1->pricesStatisticsForMonthly($marketid1),
                'prices3Monthly1' => $product1->pricesStatisticsFor3Monthly($marketid1),
                'prices6Monthly1' => $product1->pricesStatisticsFor6Monthly($marketid1),
                'pricesYearly1' => $product1->pricesStatisticsForYearly($marketid1),

                'pricesDaily2' => $product1->pricesStatisticsForDaily($marketid2),
                'pricesWeekly2' => $product1->pricesStatisticsForWeekly($marketid2),
                'pricesMonthly2' => $product1->pricesStatisticsForMonthly($marketid2),
                'prices3Monthly2' => $product1->pricesStatisticsFor3Monthly($marketid2),
                'prices6Monthly2' => $product1->pricesStatisticsFor6Monthly($marketid2),
                'pricesYearly2' => $product1->pricesStatisticsForYearly($marketid2),
            ];

            return Success::fill(['comparison' => $productResult])->send();
        }

    }


}