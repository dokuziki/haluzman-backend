<?php

/**
 * Created by PhpStorm.
 * User: coskudemirhan
 * Date: 09/06/16
 * Time: 15:01
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\NotFound;
use App\Http\Responses\Success;
use App\Models\Catalog;
use App\Models\Market;
use App\Models\News;
use App\Models\Score;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    public function getIndex()
    {
        return 'ok';
    }

    public function getMarkets()
    {
        $markets = Market::all();
        return Success::fill(['markets' => $markets])->send();
    }

    public function getFavorites()
    {


        $newsIDs = explode(',',Input::get('news'));


        $news = News::orderBy('id', 'DESC')->whereIn('id',$newsIDs)->get();
        $categories = [
            1 => 'Halden',
            2 => 'Haldekiler',
            3 => 'Uzman',
            4 => 'Dunyadan',
            5 => 'Galeri',
            6 => 'DunyaHalleri',
            7 => 'UzmanBakisi',
            8 => 'HaldenHaberler',
        ];

        $newsResult = [];

        foreach($news as $item){
            $newsResult[] = array_merge( $item->toArray(),['category' => $categories[$item->type]]);
        }


        $productIds = explode(',',Input::get('products'));

        $MarketController = new  MarketController();

        $productResult = [];

        foreach($productIds as $item){
            $data = explode('-',$item);
            if(count($data)>1)
            $productResult[]  = $MarketController->getSingleProduct($data[0],$data[1]);
        }


        $marketIds =  explode(',',Input::get('markets'));

        $markets = Market::whereIn('id',$marketIds)->get();


        return Success::fill(['news' => (Array) $newsResult,'products' => $productResult,'markets' => $markets])->send();

    }

    public function postScore(){

        $user = Auth::user();

        if($user !== null){

            $score = Score::create([
                'score' => Input::get('score'),
                'user_id' => $user->id
            ]);

            return Success::fill(['score' => $score])->send();
        }else{
            return NotFound::message('Aradığınız Kullanıcı Bulunamadı')->send();
        }
    }

    public function getScore(){

        $user = Auth::user();

        if($user !== null){


            $sub = Score::orderBy('score','DESC');


            $scoresResult = DB::table(DB::raw("({$sub->toSql()}) as sub"))
                ->groupBy('user_id')
                ->orderBy('score','DESC')
                ->take(10)
                ->get();

            $currentScore = Score::where('user_id',$user->id)->orderBy('score','DESC')->first();

            $scores = [];


            $inTen = false;
            foreach($scoresResult as $item){
                $uTemp = User::find($item->user_id);

                $current = false;

                if($currentScore !== null && $currentScore->user_id === $item->user_id){
                    $current = true;
                    $inTen = true;
                }

                $scores[] = [
                    'rank' => $item->score,
                    'name' => ucfirst($uTemp->first_name). ' '. mb_strtoupper(substr($uTemp->last_name,0,1)). '.',
                    'current' => $current
                ];
            }

            if($currentScore !== null && !$inTen){
                $uTemp = User::find($currentScore->user_id);

                $scores[] = [
                    'rank' => $currentScore->score,
                    'name' => ucfirst($uTemp->first_name). ' '. mb_strtoupper(substr($uTemp->last_name,0,1)). '.',
                    'current' => true
                ];
            }

            return Success::fill(['scores' => $scores])->send();
        }else{
            return NotFound::message('Aradığınız Kullanıcı Bulunamadı')->send();
        }
    }

    public function getTerms(){
        return view('out.terms');
    }

    public function getPrivacy(){
        return view('out.privacy');
    }

    public function getCatalogs()
    {
        $catalogs = Catalog::all();
        return Success::fill(['catalogs' => $catalogs])->send();
    }
}