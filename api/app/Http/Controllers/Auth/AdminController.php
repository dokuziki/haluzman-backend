<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;



class AdminController extends Controller
{

    public function getLogin() {
        if (Auth::check()) {
            return redirect()->route('admin.dash');
        }

        return view('admin.auth.index');
    }

    public function postLogin()
    {
        $input = [
            'email' => Input::get('email'),
            'password' => Input::get('password')
        ];

        $user = User::where('email', $input['email'])->first();

        if ($user['email'] !== $input['email']) {
            return [
                'status' => false,
                'message' => config('messages.admin.userNotFound')
            ];
        }

        if (Hash::check($input['password'], $user->password)) {
            if ($user['admin'] == 1) {
                Auth::login($user, true);


                if (Auth::user()->status == 0) {
                    Auth::logout();
                    return [
                        'status' => false,
                        'message' => config('messages.admin.unauthorized')
                    ];
                }


                return [
                    'status' => true,
                    'message' => ''
                ];

            } else {
                return [
                    'status' => false,
                    'message' => config('messages.admin.loginFailed')
                ];
            }
        }
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->action('Auth\AdminController@getLogin');
    }
}
