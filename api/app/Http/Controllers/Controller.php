<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function cacheDbResult($key, $query, $expire = 10)
    {

        if (!Cache::has($key)) {
            $result = Cache::remember($key, $expire, function () use ($query) {
                return $query->get();
            });
        } else {
            $result = Cache::get($key);
        }

        return $result;
    }

    public function userAttr($attr)
    {
        return Auth::user()->$attr;
    }

    public function inputValidation($cases)
    {
        $input = Input::all();

        $messages = [
            'required' => Config::get('messages.error.required')
        ];

        return Validator::make($input, $cases, $messages);
    }
}
