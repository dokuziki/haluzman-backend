<?php
/**
 * Created by PhpStorm.
 * User: coskudemirhan
 * Date: 07/03/16
 * Time: 20:16
 */


namespace App\Library\Push;


use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Stream;

class OneSignal
{

    const API_URL = "https://onesignal.com/api/v1";
    private $client;
    private $headers;
    private $appId;
    private $restApiKey;
    private $userAuthKey;

    public function __construct($appId, $restApiKey, $userAuthKey)
    {
        $this->appId = $appId;
        $this->restApiKey = $restApiKey;
        $this->userAuthKey = $userAuthKey;

        $this->client = new Client();
        $this->headers = ['headers' => []];
    }

    public function testCredentials()
    {
        return "APP ID: " . $this->appId . " REST: " . $this->restApiKey;
    }

    public function sendNotificationToUser($message, $userId, $url = null, $data = null)
    {
        $contents = array(
            "en" => $message
        );

        $params = array(
            'app_id' => $this->appId,
            'contents' => $contents,
            'include_player_ids' => array($userId)
        );

        if (isset($url)) {
            $params['url'] = $url;
        }

        if (isset($data)) {
            $params['data'] = $data;
        }

        $this->sendNotificationCustom($params);
    }

    /**
     * Send a notification with custom parameters defined in
     * https://documentation.onesignal.com/v2.0/docs/notifications-create-notification
     * @param array $parameters
     * @return mixed
     */
    public function sendNotificationCustom($parameters = [])
    {
        $this->requiresAuth();
        $this->usesJSON();

        // Make sure to use app_id
        $parameters['app_id'] = $this->appId;

        // Make sure to use included_segments
        if (empty($parameters['included_segments']) && empty($parameters['include_player_ids'])) {
            $parameters['included_segments'] = ['all'];
        }

        $this->headers['body'] = json_encode($parameters);
        $this->headers['verify'] = false;
        return $this->post("notifications");
    }

    private function requiresAuth()
    {
        $this->headers['headers']['Authorization'] = 'Basic ' . $this->restApiKey;
    }

    private function usesJSON()
    {
        $this->headers['headers']['Content-Type'] = 'application/json';
    }

    public function post($endPoint)
    {
        $response = $this->client->post(self::API_URL . "/" . $endPoint, $this->headers);
        return json_decode($response->getBody()->getContents());

    }

    public function sendNotificationToAll($message, $url = null, $data = null)
    {
        $contents = [
            "en" => $message,
            "tr" => $message
        ];

        $params = array(
            'app_id' => $this->appId,
            'contents' => $contents,
            'included_segments' => array('All')
        );

        if (isset($url)) {
            $params['url'] = $url;
        }

        if (isset($data)) {
            $params['data'] = $data;
        }

        return $this->sendNotificationCustom($params);
    }

    public function sendNotificationToSegment($message, $segment, $url = null, $data = null)
    {
        $contents = array(
            "en" => $message
        );

        $params = array(
            'app_id' => $this->appId,
            'contents' => $contents,
            'included_segments' => [$segment]
        );

        if (isset($url)) {
            $params['url'] = $url;
        }

        if (isset($data)) {
            $params['data'] = $data;
        }

        $this->sendNotificationCustom($params);
    }

    public function getAppDetails($parameters = [])
    {
        $this->requiresUserAuth();
        $this->usesJSON();

        $this->headers['body'] = json_encode($parameters);
        $this->headers['verify'] = false;
        $response = $this->get("apps/" . $this->appId);

        dd($response);
    }

    private function requiresUserAuth()
    {
        $this->headers['headers']['Authorization'] = 'Basic ' . $this->userAuthKey;
    }

    public function get($endPoint)
    {

        $response = $this->client->get(self::API_URL . "/" . $endPoint, $this->headers);

        return json_decode($response->getBody()->getContents());
    }

    public function getPlayers($parameters = [])
    {
        $this->requiresAuth();
        $this->usesJSON();

        $parameters['app_id'] = $this->appId;
        $parameters['limit'] = 100;
        $parameters['offset'] = 0;

        $this->headers['body'] = json_encode($parameters);
        $this->headers['verify'] = false;
        $response = $this->get("players");

        dd($response);
    }

    public function addPlayer($parameters = [])
    {
        $this->requiresAuth();
        $this->usesJSON();

        $parameters['app_id'] = $this->appId;


        $this->headers['body'] = json_encode($parameters);
        $this->headers['verify'] = false;
        $response = $this->post("players");

        if ($response->success) {
            return $response->id;
        } else {
            return false;
        }
    }

    public function sendPushToUser($parameters = [])
    {
        $this->requiresAuth();
        $this->usesJSON();

        $parameters['app_id'] = $this->appId;


        $this->headers['body'] = json_encode($parameters);
        $this->headers['verify'] = false;
        $response = $this->post("notifications");


        if ($response->id) {
            return $response->id;
        } else {
            return false;
        }
    }

    public function getPushNotifications($page)
    {
        $this->requiresAuth();
        $this->usesJSON();
        $count = 10;

        $parameters['app_id'] = $this->appId;
        $parameters['limit'] = $count;
        $parameters['offset'] = $page * $count;


        $this->headers['body'] = json_encode($parameters);
        $this->headers['verify'] = false;
        $response = $this->get('notifications');

        if (count($response->notifications) > 0) {
            return $response->notifications;
        } else {
            return false;
        }

    }

    public function addUserToPlayer($onesignalid, $userid)
    {
        $this->requiresAuth();
        $this->usesJSON();

        $parameters['app_id'] = $this->appId;
        $parameters['id'] = $onesignalid;
        $parameters['tags'] = ['user_id' => $userid];


        $this->headers['body'] = json_encode($parameters);
        $this->headers['verify'] = false;
        $response = $this->put("players" . '/' . $onesignalid);

        if ($response->success) {
            return true;
        } else {
            return false;
        }
    }

    public function put($endPoint)
    {
        $response = $this->client->put(self::API_URL . "/" . $endPoint, $this->headers);
        return json_decode($response->getBody()->getContents());

    }
}