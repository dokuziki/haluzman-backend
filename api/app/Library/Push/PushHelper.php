<?php
/**
 * Created by PhpStorm.
 * User: coskudemirhan
 * Date: 01/09/16
 * Time: 15:36
 */

namespace App\Library\Push;


class PushHelper
{
    private static $onesignal;

    public static function __callStatic($method, $arguments)
    {
        if (method_exists(__CLASS__, $method)) {
            self::getInstance();
            return forward_static_call_array(array(__CLASS__,$method),$arguments);
        }
    }

    public static function getInstance()
    {
        $config = [
            'app_id' => config('onesignal.app_id'),
            'rest_api_key' => config('onesignal.rest_api_key'),
            'user_auth_key' => config('onesignal.user_auth_key'),
        ];


        if (null === static::$onesignal) {
            static::$onesignal = new OneSignal($config['app_id'], $config['rest_api_key'], $config['user_auth_key']);
        }

        return static::$onesignal;
    }

    private static function addPlayer($registrationid){

        $deviceType = $GLOBALS['client']->type === 'android' ? 1:0;

        $parameters = [
            'device_type' => $deviceType, //0 = iOS, 1 = Android,
            'identifier' => $registrationid,//registration id
            'language' => $GLOBALS['client']->language,
            'game_version' => $GLOBALS['client']->app_version,
            'test_type' => 1 //  1 Development, 2
        ];

        return static::$onesignal->addPlayer($parameters);
    }

    private static function sendNewsPush($news){
        $data = [
            'type' => 'news',
            'id' => $news->id
        ];
        return static::$onesignal->sendNotificationToAll($news->title,null,$data);
    }


}