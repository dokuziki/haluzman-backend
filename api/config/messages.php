<?php
/**
 * Created by PhpStorm.
 * User: coskudemirhan
 * Date: 09/06/16
 * Time: 14:59
 */

return [
    'error' => [
        'unknown'         => 'Bilinmeyen bir hata oluştu.',
        'missedData'      => 'Eksik Veri.',
        'missedToken'     => 'Eksik Token.',
        'invalidRequest'  => 'Doğrulanamayan çağrı.',
        'required'        => ':attribute alanı boş bırakılamaz.',
        'unique'          => ':attribute alanı daha önce kullanılmış.',
        'min6Pass'        => 'Şifre en az 6 karakter olmalıdır.',
        'invalidEmail'    => 'Geçersiz mail adresi.',
        'userCreate'      => 'Kullanıcı kaydedilirken bir sorun oluştu.',
        'userLogin'       => 'Hatalı kullanıcı adı ya da şifre.',
        'productNotFound' => 'Aradığınız ürün bulunamadı',
        'addressNotFound' => 'Aradığınız adres bulunamadı',
        'categoryNotFound'=> 'Aradığınız kategori bulunamadı',
        'invalidFBToken'  => 'Facebook hesabınız yetkilendirilmedi',
        'emptyEmail'      => 'E-posta alanı boş bırakılamaz.',
        'fbEmptyEmail'    => 'Facebook Hesabından E-Posta Adresi Alınamadı',
        'updatePhoto'     => 'Profil fotoğrafınız güncellenemedi',
        'updateProfile'   => 'Kullanıcı bilgileri güncellenemedi',
        'missedAuth'      => 'Kullanıcı login değil',
        'login'           => 'Öncelikle kullanıcı girişi yapmanız gerekmektedir.',
        'loginFailed'     => 'Kullanıcı girişi başarısız.',
        'comment'         => 'Yorum eklenirken bir sorun oluştu.',
        'emailInUse'      => 'Bu e-posta adresi bir başka kullanıcıya ait',
        'newPasswordSend' => 'Giriş bilgilerinizi güncellemeniz için email gönderildi.',
        'facebookUser'    => 'Facebook ile giriş yaptığınız için yeni şifre talep edemezsiniz..'
    ],

    'admin' => [
        'unauthorized'    => 'Bu sayfayı görüntüleme yetkiniz bulunmamaktadır.',
        'loginFailed'     => 'Giriş bilgileri uyuşmuyor.',
        'formError'       => 'İşlem yaptığınız formda bir hata oluştu.',
        'unknownError'    => 'Bilinmeyen bir hata oluştu.',
        'confirmDelete'   => 'Bu kaydı silmek istediğinizden emin misiniz ?',
        'editUserTrue'    => 'Üye düzenlendi.',
        'editUserFalse'   => 'Üye düzenlenirken bir hata meydana geldi.',
        'deleteUser'      => 'Üye silindi',
        'userNotFound'    => 'Kullanıcı Bulunamadı.'

    ]
];