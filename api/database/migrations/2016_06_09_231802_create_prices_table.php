<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('min');
            $table->string('max');
            $table->date('date');
            $table->integer('product_id')->unsigned();
            $table->integer('market_id')->unsigned();
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('market_id')->references('id')->on('markets')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        if (Schema::hasTable('prices')) {

            Schema::table('prices', function (Blueprint $table) {
                $table->dropForeign('prices_product_id_foreign');
                $table->dropForeign('prices_market_id_foreign');
            });

            Schema::drop('prices');
        }
    }
}
