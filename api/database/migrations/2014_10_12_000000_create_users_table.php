<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('fb_id',191)->default("");
            $table->text('fb_token')->nullable();
            $table->string('email', 191)->unique();
            $table->string('password', 191);
            $table->date('birthday')->nullable();
            $table->string('phone')->nullable();
            $table->string('pic',200)->default("");


            $table->integer('gender')->unsigned()->default(2);
            $table->integer('newsletter')->enum(2);
            $table->integer('status')->unsigned();
            $table->integer('admin')->default(0)->unsigned();

            $table->rememberToken();
            $table->index('remember_token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table)
        {
            $table->dropIndex(['remember_token']);
        });

        Schema::drop('users');
    }
}
