<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('markets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('location');
            $table->string('address');
            $table->string('phone');

            $table->timestamps();
        });

        Schema::create('market_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('market_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->string('crawl_name');

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('market_id')->references('id')->on('markets')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('market_products')) {

            Schema::table('market_products', function (Blueprint $table) {
                $table->dropForeign('market_products_product_id_foreign');
                $table->dropForeign('market_products_market_id_foreign');
            });

            Schema::drop('market_products');
            Schema::drop('markets');
        }

    }
}
