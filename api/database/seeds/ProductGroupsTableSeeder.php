<?php

use Illuminate\Database\Seeder;

class ProductGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $productGroups = [
            ['title' => 'Domates', 'image' => 'http://placehold.it/200x200'],
            ['title' => 'Biber', 'image' => 'http://placehold.it/200x200'],
            ['title' => 'Hıyar', 'image' => 'http://placehold.it/200x200'],
            ['title' => 'Marul', 'image' => 'http://placehold.it/200x200'],
            ['title' => 'Patlıcan', 'image' => 'http://placehold.it/200x200'],
            ['title' => 'Karpuz', 'image' => 'http://placehold.it/200x200']
        ];

        App\Models\ProductGroup::insert($productGroups);
    }
}
