<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [

            [
                'first_name' => 'Coşku',
                'last_name' => 'Demirhan',
                'email' => 'cosku@dokuziki.com',
                'password' => bcrypt('12345'),
                'admin' => 1,
                'status' => 1
            ],
            [
                'first_name' => 'Okan',
                'last_name' => 'Yüksel',
                'email' => 'okan.yuksel@ward.agency',
                'password' => bcrypt('12345'),
                'admin' => 1,
                'status' => 1
            ],
            [
                'first_name' => 'admin',
                'last_name' => 'test',
                'email' => 'admin@dokuziki.com',
                'password' => bcrypt('admin'),
                'admin' => 1,
                'status' => 1
            ]
        ];


        \App\Models\User::insert($data);
    }
}
