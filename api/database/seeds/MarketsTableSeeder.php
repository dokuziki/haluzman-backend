<?php

use Illuminate\Database\Seeder;

class MarketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $markets = [
            ['title' => 'İstanbul / Ataşehir'],
            ['title' => 'İstanbul / Bayrampaşa'],
            ['title' => 'Antalya / Merkez'],
            ['title' => 'Antalya / Kumluca'],
            ['title' => 'İzmir'],
            ['title' => 'Mersin'],
            ['title' => 'Karaçulha'],

        ];

        App\Models\Market::insert($markets);
    }
}
