<?php

use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [

            [
                'title' => 'Sıcak hava fiyatları düşürdü',
                'thumb' => 'http://placehold.it/200x200',
                'img' => 'http://placehold.it/200x200',
                'description' => 'Türkiye\'nin en büyük sebze üreticilerinden biri olan Antalya\'da havanın sıcaklığının artması nedeniyle hallerdeki sebze fiyatları düştü.',
                'text' => 'Türkiye\'nin en büyük sebze üreticilerinden biri olan Antalya\'da havanın sıcaklığının artması nedeniyle hallerdeki sebze fiyatları düştü. Antalya Toptancı Haller Müdürü Dilaver Demir, yaptığı açıklamada “Son bir haftadır havaların aşırı derecede sıcak gitmesiyle çiftçilerimiz yetiştirdikleri ürünleri normal şartlarda 2 veya 3 gün toplayıp hale getirirken, son dönemde neredeyse her gün getirmeye başladılar” diyerek arz ve talebe göre belirlenen fiyatlarda ürünün fazla olmasıyla düşüşlerin yaşandığını vurguladı. <br /> <b>En düşük domates 1 lira</b> <br/> Bu düşüşün mevsimsel olduğuna dikkat çeken Demir, haldeki sebze fiyatlarının son durumu hakkında da şunları söyledi: “Mesela dolma biberin satış fiyatı en düşük 70 kuruş, en yüksek 1 TL. Ana kalem ürünümüz olan domatesin kilogram fiyatı 1 TL ile 1,5 TL arasında değişen fiyatlarda halimize satılıyor. Salatalık 50 kuruş ile 1 lira arasında, kabak 70 kuruş ile 1,10 TL arasında, patlıcanın fiyatı ise 50 kuruş ile 1 TL aralığında şu an toptancı halimizde işlem görüyor.”',
                'source' => '',
                'type' => 1
            ],
            [
                'title' => 'Balıkesir Hali’nde kuralar çekildi',
                'thumb' => 'http://placehold.it/200x200',
                'img' => 'http://placehold.it/200x200',
                'description' => 'Balıkesir Büyükşehir Belediyesi tarafından inşası devam eden Yaş Sebze ve Meyve Toptancı Hali’nde faaliyet gösterecek olan esnaf, yeni iş yerleri için kura çekti.',
                'text' => 'Balıkesir Büyükşehir Belediyesi tarafından inşası devam eden Yaş Sebze ve Meyve Toptancı Hali’nde faaliyet gösterecek olan esnaf, yeni iş yerleri için kura çekti. Noter huzurunda gerçekleştirilen törende 60 iş yeri için kura çekim işlemi gerçekleştirildi. Kura çekim törenine Balıkesir Büyükşehir Belediyesi Genel Sekreter Yardımcısı Aydın Pehlivan, Kırsal Hizmetler Dairesi Başkanı Ahmet Mekin Tüzün, hal esnafı ve vatandaşlar katıldı. Salih Tozan Kültür Merkezi’nde düzenlenen ve Balıkesir 2. Noteri huzurunda gerçekleştirilen törende Kırsal Hizmetler Daire Başkanı Ahmet Mekin Tüzün’ün konuşmasının ardından alfabetik sıraya göre kura çeken hal esnafı önce kura çekim sırasını belirledi. Kura çekimleri ardından esnaf yerleri için ikinci bir kura çekerek yeni dükkanlarını kendileri tespit etti.<b>45,8 milyon TL’ye mal olacak</b><br />Nisan ayında temeli atılan ve Kasım ayında hizmete girmesi planlanan yeni hal binası 45 milyon 843 bin TL’ye mal olacak. Hal binasının inşaat alanı 110 bin metrekarelik yüzölçümüne sahip. Balıkesir Büyükşehir Belediyesi Yaş Sebze ve Meyve Hali’nin içerisinde; 1 adet idari bina, 60 adet ticari birim (hal dükkanı), 4 adet sosyal tesis, 60 adet kasa deposu, 2 adet kasa yıkama ünitesi, 1 adet danışma bürosu, 1 adet kantar binası, 1 adet oto yıkama ünitesi, 3 adet patates yıkama ünitesi, 1 adet su deposu, 2 adet paket arıtma ünitesi, 1 adet soğuk hava deposu (1600 metrekarede 11 bin 200 metreküplük), cami ve şadırvan için rezerv alan bulunacak.',
                'source' => '',
                'type' => 1
            ],
            [
                'title' => 'Pazarkapı’daki hal yıkılıyor',
                'thumb' => 'http://placehold.it/200x200',
                'img' => 'http://placehold.it/200x200',
                'description' => 'Trabzon Büyükşehir Belediyesi, TOKİ işbirliğiyle yürüttüğü kentsel dönüşüm projesi kapsamında Pazarkapı’da genelde kadınların kullandığı meyve ve sebze halini yıkma kararı aldı.',
                'text' => 'Trabzon Büyükşehir Belediyesi, TOKİ işbirliğiyle yürüttüğü kentsel dönüşüm projesi kapsamında Pazarkapı’da genelde kadınların kullandığı meyve ve sebze halini yıkma kararı aldı. 1988’de kurulan Trabzon Büyükşehir Belediyesi Meyve ve Sebze Hali yüzü aşkın esnafa ev sahipliği yaparken; pazarda aynı zamanda köylülerde kendi topladıkları meyve sebzelerini aracılar aracılığıyla satıyordu. <br /><b>Esnaf tedirgin</b><br />Esnaf Belediyenin kendilerine kesin bir bilgi vermemesinden dolayı şikayet ederken; yıkım çalışmaları yapılmaya başlandığında kendilerine tahsis edilecek bir yer gösterilmeyen esnaf mağdur olmak istemiyor.',
                'source' => '',
                'type' => 1
            ],
            [
                'title' => 'Antalya Hali umutlu',
                'thumb' => 'http://placehold.it/200x200',
                'img' => 'http://placehold.it/200x200',
                'description' => 'Rusya ile yaşanan yumuşama süreci Antalya Hali’nde sevinçle karşılandı. ',
                'text' => 'Rusya ile yaşanan yumuşama süreci Antalya Hali’nde sevinçle karşılandı. Türkiye ve Rusya arasında ekonomik ve ticari ilişkilerin yeniden geliştirilmesi sürecini değerlendiren Antalya Toptancı Hal Yaş Meyve Sebze Komisyoncuları Derneği Başkanı Cüneyt Doğan, Rusya ile yapılan görüşmelerin sonucunu üretici ve tüccarın merakla beklediğini ifade etti. Her olumlu gelişmenin kendilerini daha da aydınlatacağını söyleyen Doğan, “Üretici gelecek sezon adına ne ekeceğine kararsızdı. Bu kararsızlık, kararlılığa yönelebilir. Üretici Rusya pazarına yönelik ekim yapabilir. Rusya için ekilen dikenli salatalık yeniden dikilebilir. Domatesten kaçma olacak gibiydi, yeniden bir dönüş olabilir” diye konuştu. ',
                'source' => '',
                'type' => 1
            ],
            [
                'title' => 'Yozgat Sebze Hali’nde inşaata başlanıyor',
                'thumb' => 'http://placehold.it/200x200',
                'img' => 'http://placehold.it/200x200',
                'description' => 'Yozgat’ta daha önce ihalesi yapılan, ancak yüklenici firmanın yükümlülüklerini yerine getirmemesi nedeniyle iptal edilen “Şehir Meydanı Projesi” ile ilgili yeni ihale süreci tamamlandı.',
                'text' => 'Yozgat’ta daha önce ihalesi yapılan, ancak yüklenici firmanın yükümlülüklerini yerine getirmemesi nedeniyle iptal edilen “Şehir Meydanı Projesi” ile ilgili yeni ihale süreci tamamlandı. Gerekli işlemlerin ardından yaklaşık bir ay içerisinde sebze halinde çalışmalara başlanılarak, 550 gün içerisinde de tamamlanmasının planlandığı belirtildi. Yozgat Belediye Başkanı Kazım Arslan, yaklaşık 40 yıl önce hizmete sunulan, bugün ise şehir merkezinin gelişmesinde ciddi engel taşıyan sebze halini ıslahına yönelik hazırlanan projenin artık hayata geçirilme aşamasına gelindiğini söyledi.<br /><b>Kamulaştırmaya gidilecek</b><br />Kazım Arslan yaptığı açıklamada Yozgat’ın önemli ihtiyaçlarından birisinin şehrin merkezinde fonksiyonunu yitirmiş olan hal binasının yenilenmesi olduğunu söyleyerek “Bu maksatla eski halin tamamen yıkılarak yerine yeni bir hal ve işyeri yapılmasını gündemimize almıştık. Anıtlar Kurulu’nu projeyi onaylamasının ardından ihalesini yaptık’’ diye konuştu. Arslan sebze halinde bulunan esnafın büyük bir kısmı ile anlaştıklarını, anlaşılama sağlanamayan kısımlarda ise kamulaştırma ile meseleyi çözeceklerini ifade etti. Gerekli işlemlerin ardından yaklaşık bir ay içerisinde sebze halinde çalışmalara başlanılarak, 550 gün içerisinde de tamamlanmasının planlandığı belirtildi. Yozgat Belediye Başkanı Kazım Arslan, yaklaşık 40 yıl önce hizmete sunulan, bugün ise şehir merkezinin gelişmesinde ciddi engel taşıyan sebze halini ıslahına yönelik hazırlanan projenin artık hayata geçirilme aşamasına gelindiğini söyledi.',
                'source' => '',
                'type' => 1
            ],
            [
                'title' => 'Çanakkale’ye yeni hal',
                'thumb' => 'http://placehold.it/200x200',
                'img' => 'http://placehold.it/200x200',
                'description' => 'Çanakkale Belediyesi Yaş Sebze ve Meyve Hali, şehir içi ve şehirlerarası ulaşımda kolaylıkla erişim sağlanabilmesi için yeni bir noktada inşa ediliyor. ',
                'text' => 'Çanakkale Belediyesi Yaş Sebze ve Meyve Hali, şehir içi ve şehirlerarası ulaşımda kolaylıkla erişim sağlanabilmesi için yeni bir noktada inşa ediliyor. Yeni hal binası için Çanakkale Belediyesi’nden yapılan açıklamada; İsmetpaşa mahallesi, İzmir caddesi üzerinde yaklaşık 10 bin m² parsel alanına sahip yaş sebze ve meyve hali projesinin, 30 adet satış birimini de içerisinde barındırdığı ve yaklaşık 5 bin m² kapalı alana sahip olacağı bilgisi paylaşıldı. <br/><b>Herkes faydalanabilecek</b><br/>Projede yeni bina; bilgi işletim sistemi, ağırlık kontrol ünitesi, güvenlik sistemi gibi teknolojik ünitelerin yanı sıra hal personelinin ve dışarıdan gelen vatandaşların da faydalanabileceği bir idari bina şeklinde tasarlandı. Bina içerisinde yer alan her bir birim ise 130 m² depo alanı ve 20 m² ofis alanı şeklinde planlandı. Yeni bina için inşaat çalışmaları devam ediyor. ',
                'source' => '',
                'type' => 1
            ]
        ];


        \App\Models\News::insert($data);
    }
}
