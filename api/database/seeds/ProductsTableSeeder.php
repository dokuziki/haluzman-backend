<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            ['group_id' => 1, 'title' => 'Domates', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 1, 'title' => 'Domates LÜX (İhracat)', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 1, 'title' => 'Yeşil Domates', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 1, 'title' => 'Kokteyl Domates', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 1, 'title' => 'Kokteyl Oval', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 1, 'title' => 'Kokteyl Salkım', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 1, 'title' => 'Kokteyl Yuvarlak', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 1, 'title' => 'Cherry Domates', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 1, 'title' => 'Salkım', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 1, 'title' => 'Beef', 'image' => 'http://placehold.it/200x200'],


            ['group_id' => 2, 'title' => 'Çarliston', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 2, 'title' => 'Sivri Biber', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 2, 'title' => 'İnce Sivri', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 2, 'title' => 'Kalın Sivri', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 2, 'title' => 'Dolma Biber', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 2, 'title' => 'Kapya', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 2, 'title' => 'Köy Biberi', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 2, 'title' => 'Kıl Biber', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 2, 'title' => 'Kaliforniya', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 2, 'title' => 'Kaliforniya Sarı', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 2, 'title' => 'Kaliforniya Kırmızı', 'image' => 'http://placehold.it/200x200'],

            ['group_id' => 3, 'title' => 'Hıyar', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 3, 'title' => 'Silor', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 3, 'title' => 'Salata Çengelköy Silor', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 3, 'title' => 'Silor Paket', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 3, 'title' => 'Silor Badem', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 3, 'title' => 'Dikenli', 'image' => 'http://placehold.it/200x200'],

            ['group_id' => 4, 'title' => 'Iceberg', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 4, 'title' => 'Kıvırcık', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 4, 'title' => 'Yedikule', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 4, 'title' => 'Lollorosso', 'image' => 'http://placehold.it/200x200'],

            ['group_id' => 5, 'title' => 'Patlıcan', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 5, 'title' => 'Bostan', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 5, 'title' => 'Kristal', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 5, 'title' => 'Tünel', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 5, 'title' => 'Uzun', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 5, 'title' => 'Topan', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 5, 'title' => 'Tophane', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 5, 'title' => 'Anamur', 'image' => 'http://placehold.it/200x200'],
            ['group_id' => 5, 'title' => 'Topak', 'image' => 'http://placehold.it/200x200'],

            ['group_id' => 5, 'title' => 'Karpuz', 'image' => 'http://placehold.it/200x200'],
        ];

        $market_products = [
            ['market_id' => 1, 'product_id' => 1],
            ['market_id' => 1, 'product_id' => 2],
            ['market_id' => 1, 'product_id' => 4],
            ['market_id' => 1, 'product_id' => 8],
            ['market_id' => 1, 'product_id' => 6],
            ['market_id' => 1, 'product_id' => 10],
            ['market_id' => 1, 'product_id' => 11],
            ['market_id' => 1, 'product_id' => 12],
            ['market_id' => 1, 'product_id' => 15],
            ['market_id' => 1, 'product_id' => 16],
            ['market_id' => 1, 'product_id' => 17],
            ['market_id' => 1, 'product_id' => 18],
            ['market_id' => 1, 'product_id' => 19],
            ['market_id' => 1, 'product_id' => 22],
            ['market_id' => 1, 'product_id' => 24],
            ['market_id' => 1, 'product_id' => 28],
            ['market_id' => 1, 'product_id' => 29],
            ['market_id' => 1, 'product_id' => 30],
            ['market_id' => 1, 'product_id' => 32],
            ['market_id' => 1, 'product_id' => 33],
            ['market_id' => 1, 'product_id' => 41]
        ];

        App\Models\Product::insert($products);
        DB::table('market_products')->insert($market_products);


    }
}
