<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(MarketsTableSeeder::class);
        $this->call(ProductGroupsTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(UserSeeder::class);

        Model::reguard();
    }
}
