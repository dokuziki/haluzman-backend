<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            [
                'object' => 'market',
                'title' => 'Lorem Ipsum Dolor Sit Amet',
                'description' => 'Sed iaculis eleifend massa, sed fermentum sem luctus nec',
                'image' => 'http://placehold.it/1242x527.jpg',
            ],
        ];

        App\Models\Setting::insert($settings);
    }
}
