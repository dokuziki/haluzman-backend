@extends('admin.layouts.master')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> {{$market->title}} Hali |  {{$product->title}} Fiyat Listesi</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column"
                           id="price">
                        <thead>
                        <tr>
                            <th><center> Gün </center></th>
                            <th><center> Üst Fiyat </center></th>
                            <th><center> Alt Fiyat </center></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($dates as $day)
                            <tr class="odd gradeX" data-date="$day">
                                <td>{{$day}}</td>
                                <td class="max-input" data-date="{{$day}}"><center>@if(isset($dateBy[$day]['max'])) {{$dateBy[$day]['max']}} @else 0 @endif TL</center></td>
                                <td class="min-input" data-date="{{$day}}"><center>@if(isset($dateBy[$day]['min'])) {{$dateBy[$day]['min']}} @else 0 @endif TL</center></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>

@endsection


@section('script')
    <script>
        $(document).ready(function() {
            $('#price').DataTable( {
                "order": [[ 0, "desc" ]]
            });


            var isloding = false;

            $('.max-input,.min-input').click(function(e){

                var target = $( e.target );


                if($('.on-fly').length > 0 || isloding){
                    // bişey yapma
                }else{
                    //input ekleme
                    var value = parseFloat($(this).text());
                    var date = $(this).data('date');

                    var html = '';

                    if(target.is( ".max-input>center>input" )|| target.is( ".max-input>center" )){
                        html = '<input type="number" value="'+value+'" class="on-fly max" data-date="'+date+'" />';
                    }else{
                        html = '<input type="number" value="'+value+'" class="on-fly min" data-date="'+date+'"  />';
                    }

                    $(this).find('center').html(html);

                }

            });


            $('body').click(function(e) {

                var target = $( e.target );

                if ( target.is( ".max-input>center" ) || target.is( ".min-input>center" ) ||  target.is( ".max-input>center>input" ) || target.is( ".min-input>center>input" )) {

                }else{
                    if($(this).find('.on-fly').length > 0) {

                        for(var i = 0; i < $(this).find('.on-fly').length; ++i){
                            var currentInput = $(this).find('.on-fly')[i];
                                sendData($(currentInput));
                        }

                    }
                }

            });


            var sendData =  function(input){

                isloding = true;

                var data =  {'product_id': "{{$product->id}}", 'market_id': "{{$market->id}}"};

                data.date = input.attr('data-date');

                if(input.hasClass('max')){
                    data.max = input.val();
                }

                if(input.hasClass('min')){
                    data.min = input.val();
                }


                console.log(data);

                $.ajax({
                    url: "{{action('Panel\ProductController@postPrice')}}",
                    data: data,
                    type: 'POST',

                    success: function (data) {
                        if (data.status) {
                            var value = input.val() + ' TL';
                            input.parents('center').html(value);
                            toastr.info("Fiyat Güncellendi");
                        }
                        else {
                            toastr.error("Fiyat Kaydedilirken Bir Sorun oluştu");
                        }

                        isloding = false;
                    },
                    error: function () {
                        toastr.error("Fiyat Kaydedilirken Bir Sorun oluştu");
                        isloding = false;
                    }
                });


            };






        });
    </script>

@endsection