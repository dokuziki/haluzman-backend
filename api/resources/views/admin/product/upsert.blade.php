@extends('admin.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="fa fa-plus"></i>
                        <span class="caption-subject bold uppercase">{{$item['title']}}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="{{action('Panel\ProductController@postUpsert')}}" enctype="multipart/form-data" method="post" class="horizontal-form">
                        <input type="hidden" name="id" @if(isset($item['id'])) value="{{$item['id']}}" @else value="false" @endif />

                        <div class="form-body">
                            <h3 class="form-section">Ürün Grubu Ayrıntıları</h3>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Başlık</label>
                                        <input type="text" id="title" name="title" class="form-control" placeholder="Başlık" value="{{$item['title']}}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Resim</label>
                                        <input type="file" id="image" name="image" class="form-control" placeholder="Resim">
                                    </div>
                                    @if(isset($item['image']) && $item['image'] !== '' &&  $item['image'] !== asset('uploads/products/'))
                                        <img height="150" src="{{$item['image']}}" />
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-actions right">
                            <button type="submit" class="btn blue">
                                <i class="fa fa-check"></i> Kaydet
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection