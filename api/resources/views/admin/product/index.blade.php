@extends('admin.layouts.master')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> @if($isGroup) Ürün Grupları @else Ürünler @endif</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table data-table table-striped table-bordered table-hover table-checkable order-column"
                           id="sample_1">
                        <thead>
                        <tr>
                            <th><center> Ürün Adı </center></th>
                            <th><center> İşlemler </center></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $product)
                            <tr class="odd gradeX">

                                <td><center> {{$product['title']}}</center></td>
                                <td>
                                    <center>
                                        @if($isGroup)
                                        <a href="{{asset('backoffice/product/detail')}}/{{$product['id']}}" data-action="add" class="btn btn-primary btn-sm ">Ürünler</a>
                                        <a href="{{asset('backoffice/product/upsert')}}/{{$product['id']}}" data-action="add" class="btn btn-primary btn-sm edit"><i class="fa fa-pencil-square-o"></i></a>
                                        @else
                                            <a href="{{asset('backoffice/product/product')}}/{{$product['id']}}" data-action="add" class="btn btn-primary btn-sm edit"><i class="fa fa-pencil-square-o"></i></a>
                                        @endif
                                    </center>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>

        @endsection
