@extends('admin.layouts.master')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="fa fa-plus"></i>
                        <span class="caption-subject bold uppercase">Yeni Haber</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="{{action('Panel\NewsController@postUpsert')}}" enctype="multipart/form-data" method="post" class="horizontal-form">
                        <input type="hidden" name="id" @if(isset($item['id'])) value="{{$item['id']}}" @else value="false" @endif />

                        <div class="form-body">
                            <h3 class="form-section">Haber Ayrıntıları</h3>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Başlık</label>
                                        <input type="text" id="title" name="title" class="form-control" placeholder="Başlık" value="{{$item['title']}}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Küçük Resim</label>
                                        <input type="file" id="thumb" name="thumb" class="form-control" placeholder="Küçük Resim">
                                    </div>
                                    @if(isset($item['thumb']) && $item['thumb'] !== '' &&  $item['thumb'] !== asset('uploads/news/'))
                                        <img height="150" src="{{$item['thumb']}}" />
                                    @endif
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Resim</label>
                                        <input type="file" id="image" name="img" class="form-control" placeholder="Resim" />
                                    </div>
                                    @if(isset($item['img']) && $item['img'] !== '')
                                        <img height="150" src="{{$item['img']}}" />
                                    @endif
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Video</label>
                                        <input type="file" id="video" name="video" class="form-control" placeholder="Video" />
                                    </div>
                                    @if(isset($item['video']) && $item['video'] !== '')
                                        <video height="150" controls>
                                            <source src="{{$item['video']}}" type="video/mp4">
                                            Your browser does not support the video tag.
                                        </video>
                                    @endif
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Açıklama</label>
                                        <input type="text" id="description" name="description" class="form-control" placeholder="Açıklama" value="{{$item['description']}}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Kaynak</label>
                                        <input type="text" id="source" name="source" class="form-control" placeholder="Kaynak" value="{{$item['source']}}">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Kategori</label>
                                        <select id="category" class="form-control" name="category">
                                            <option value="1" @if($item['type'] === 1) selected @endif>Halden Haberler</option>
                                            <option value="2" @if($item['type'] === 2) selected @endif>Uzman Bakışı - Haldekiler</option>
                                            <option value="3" @if($item['type'] === 3) selected @endif>Uzman Bakışı - Nunhems Uzmanları</option>
                                            <option value="4" @if($item['type'] === 4) selected @endif>Dünya Halleri - Dünyadan Haberler</option>
                                            <option value="5" @if($item['type'] === 5) selected @endif>Dünya Halleri - Galeri</option>
                                            <option value="6" @if($item['type'] === 6) selected @endif>Dünya Halleri - Manşet</option>
                                            <option value="7" @if($item['type'] === 7) selected @endif>Uzman Bakışı - Manşet</option>
                                            <option value="8" @if($item['type'] === 8) selected @endif>Halden Haberler - Manşet</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="control-label">Metin</label>
                                        <textarea name="content" class="summernote" id="content" placeholder="Metin" style="width: 100%">{{$item['text']}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
                        <div class="form-actions right">
                            <button type="submit" class="btn blue">
                                <i class="fa fa-check"></i> Kaydet
                            </button>
                        </div>
                    </form>
                </div>
            </div>


        </div>
    </div>

@endsection

@section('script')
<script>
    $(document).ready(function(){
        $('.summernote').summernote({
            height: 250
        });

    });
</script>
@endsection