@extends('admin.layouts.master')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Haberler</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a href="{{asset('backoffice/news/upsert')}}" id="sample_editable_1_new"
                                       class="btn sbold green"> Yeni Haber
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <table class="table data-table table-striped table-bordered table-hover table-checkable order-column"
                           id="sample_1">
                        <thead>
                        <tr>
                            <th> ID</th>
                            <th> Başlık</th>
                            <th> Kategori</th>
                            <th> İşlemler</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($news as $new)
                            <tr class="odd gradeX">
                                <td> {{$new['id']}}</td>
                                <td> {{$new['title']}}</td>
                                <td>
                                    @if($new['type'] == 1)
                                        <span class="label label-sm label-danger"> Halden Haberler </span>
                                    @elseif($new['type'] == 2)
                                        <span class="label label-sm label-danger"> Uzman Bakışı - Haldekiler </span>
                                    @elseif($new['type'] == 3)
                                        <span class="label label-sm label-danger"> Uzman Bakışı - Nunhems Uzmanları </span>
                                    @elseif($new['type'] == 4)
                                        <span class="label label-sm label-danger"> Dünya Halleri - Dünyadan Haberler </span>
                                    @elseif($new['type'] == 5)
                                        <span class="label label-sm label-danger"> Dünya Halleri - Galeri </span>
                                    @elseif($new['type'] == 6)
                                        <span class="label label-sm label-danger"> Dünya Halleri - Manşet </span>
                                    @elseif($new['type'] == 7)
                                        <span class="label label-sm label-danger"> Uzman Bakışı Manşet </span>
                                    @elseif($new['type'] == 8)
                                        <span class="label label-sm label-danger"> Halden Haberler - Manşet </span>
                                    @endif
                                </td>
                                <td>
                                    @if((int)$new['push'] === 0)
                                        <a href="{{asset('backoffice/news/push')}}/{{$new['id']}}" title="Bildirim Gönder"
                                           class="btn btn-primary btn-sm "><i class="fa fa-envelope-o"></i></a>
                                    @else
                                        <button class="btn btn-primary btn-sm edit"
                                                data-deleted="tr.gradeX" disabled><i class="fa fa-envelope-o"></i></button>
                                    @endif
                                    <button class="btn btn-sm btn-danger deleteOpenModal" data-id="{{$new['id']}}"
                                            data-deleted="tr.gradeX"><i class="fa fa-trash"></i></button>
                                    <a href="{{asset('backoffice/news/upsert')}}/{{$new['id']}}" data-action="add"
                                       class="btn btn-primary btn-sm edit"><i class="fa fa-pencil-square-o"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>


        <div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">{{config('messages.admin.confirmDelete')}}</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default close-modal" data-dismiss="modal">Vazgeç</button>
                        <submit type="button" class="btn btn-danger delete" id="newEditorAdd">Haberi Sil</submit>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('script')
            <script>
                $(document).ready(function () {

                    $("#mask_date").inputmask("d/m/y", {
                        autoUnmask: true
                    });

                    var body = $('body');


                    body.on('click', '.deleteOpenModal', function (event) {
                        $('#deleteConfirm').modal('show');
                        var dataID = $(this).attr('data-id');

                        var button = $(this);
                        var deleted = $(this).attr('data-deleted');

                        $('.delete').click(function () {
                            window.location = '{{action('Panel\NewsController@getDelete')}}' + '/' + dataID;
                        });

                    });
                });


            </script>
@endsection