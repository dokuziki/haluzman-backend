@extends('admin.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="fa fa-plus"></i>
                        <span class="caption-subject bold uppercase">{{$item['title']}}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="{{action('Panel\MarketController@postUpsert')}}" enctype="multipart/form-data" method="post" class="horizontal-form">
                        <input type="hidden" name="id" @if(isset($item['id'])) value="{{$item['id']}}" @else value="false" @endif />

                        <div class="form-body">
                            <h3 class="form-section">Hal Ayrıntıları</h3>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Başlık</label>
                                        <input type="text" id="title" name="title" class="form-control" placeholder="Başlık" value="{{$item['title']}}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Konum</label>
                                        <input type="text" id="location" name="location" class="form-control" placeholder="Konum" value="{{$item['location']}}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Telefon</label>
                                        <input type="text" id="phone" name="phone" class="form-control" placeholder="Telefon" value="{{$item['phone']}}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Adres</label>
                                        <textarea class="form-control" name="address" id="address" rows="3">{{$item['address']}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions right">
                            <button type="submit" class="btn blue">
                                <i class="fa fa-check"></i> Kaydet
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection