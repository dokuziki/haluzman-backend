@extends('admin.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="fa fa-plus"></i>
                        <span class="caption-subject bold uppercase">{{$item['name']}}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="{{action('Panel\MarketController@postPerson')}}" enctype="multipart/form-data" method="post" class="horizontal-form">
                        <input type="hidden" name="id" @if(isset($item['id'])) value="{{$item['id']}}" @else value="false" @endif />
                        <input type="hidden" name="market_id" @if(isset($market_id)) value="{{$market_id}}" @else value="{{$item['market_id']}}" @endif />
                        <div class="form-body">
                            <h3 class="form-section">Yetkili Bilgileri</h3>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Adı Soyadı</label>
                                        <input type="text" id="name" name="name" class="form-control" placeholder="Adı Soyadı" value="{{$item['name']}}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Telefon</label>
                                        <input type="text" id="phone" name="phone" class="form-control" placeholder="Telefon" value="{{$item['gsm']}}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Resim</label>
                                        <input type="file" id="image" name="image" class="form-control" placeholder="Resim">
                                    </div>
                                    @if(isset($item['pic']) && $item['pic'] !== '' &&  $item['pic'] !== asset('uploads/persons/'))
                                        <img height="150" src="{{$item['pic']}}" />
                                    @endif
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div class="form-actions right">
                            <button type="submit" class="btn blue">
                                <i class="fa fa-check"></i> Kaydet
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection