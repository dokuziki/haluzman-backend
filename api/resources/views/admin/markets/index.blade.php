@extends('admin.layouts.master')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> @if($isGroup) Haller @else Hal Yetkilileri @endif</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table data-table table-striped table-bordered table-hover table-checkable order-column"
                           id="sample_1">
                        @if($isGroup)
                        @else
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{asset('backoffice/markets/person')}}/{{$market_id}}" id="sample_editable_1_new" class="btn sbold green"> Yetkili Ekle
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <br>
                        @endif
                        <thead>
                        <tr>
                            <th><center>@if($isGroup) Hal Adı @else Adı Soyadı @endif </center></th>
                            <th><center> İşlemler </center></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr class="odd gradeX">

                                <td><center>@if($isGroup) {{$item['title']}} @else {{$item['name']}} @endif</center></td>
                                <td>
                                    <center>
                                        @if($isGroup)
                                            <a href="{{asset('backoffice/markets/detail')}}/{{$item['id']}}" data-action="add" class="btn btn-primary btn-sm ">Hal Yetkilileri</a>
                                            <a href="{{asset('backoffice/markets/upsert')}}/{{$item['id']}}" data-action="add" class="btn btn-primary btn-sm edit"><i class="fa fa-pencil-square-o"></i></a>
                                        @else
                                            <button class="btn btn-sm btn-danger deleteOpenModal" data-id="{{$item['id']}}" data-deleted="tr.gradeX"><i class="fa fa-trash"></i></button>
                                            <a href="{{asset('backoffice/markets/person')}}/{{$market_id}}/{{$item['id']}}" data-action="add" class="btn btn-primary btn-sm edit"><i class="fa fa-pencil-square-o"></i></a>
                                        @endif
                                    </center>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">{{config('messages.admin.confirmDelete')}}</h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default close-modal" data-dismiss="modal">Vazgeç</button>
                                    <submit type="button" class="btn btn-danger delete" id="newEditorAdd">Yetkiliyi Sil</submit>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
</div>

@endsection

@section('script')
    <script>
        $(document).ready(function () {

            $("#mask_date").inputmask("d/m/y", {
                autoUnmask: true
            });

            var body = $('body');



            body.on('click','.deleteOpenModal',function(event){
                $('#deleteConfirm').modal('show');
                var dataID = $(this).attr('data-id');

                var button = $(this);
                var deleted = $(this).attr('data-deleted');

                $('.delete').click(function (){
                    window.location= '{{action('Panel\MarketController@getDelete')}}'+'/'+dataID;
                });

            });
        });


    </script>
@endsection
