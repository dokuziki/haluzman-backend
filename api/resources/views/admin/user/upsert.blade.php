@extends('admin.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Kullanıcı Ayarı</span>
                    </div>

                </div>
                <div class="portlet-body">

                    <!-- BEGIN FORM-->
                    <form action="{{asset('backoffice/user/upsert')}}@if(isset($item['id']))/{{$item['id']}}@endif" method="post" class="horizontal-form">
                            <input type="hidden" name="id"   @if(isset($item['id'])) value="{{$item['id']}}" @else value="false" @endif />

                        <div class="form-body">
                            <h3 class="form-section">Hesap Bilgileri</h3>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Adı</label>
                                        <input type="text" name="first_name" class="form-control" placeholder="" @if(isset($item['first_name'])) value="{{$item['first_name']}}" @endif autocomplete="new-password" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Soyadı</label>
                                        <input type="text" name="last_name" class="form-control" placeholder="" @if(isset($item['last_name'])) value="{{$item['last_name']}}" @endif autocomplete="new-password" required>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Mail Adresi</label>
                                        <input type="text" name="email" class="form-control" placeholder="" @if(isset($item['email'])) value="{{$item['email']}}" @endif autocomplete="new-password" required>
                                    </div>
                                </div>
                                <!--/span-->

                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Şifre</label>
                                        <input type="password" name="password" class="form-control" placeholder="" @if(isset($item['password'])) value="" @endif autocomplete="new-password" >
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <br>
                            <br>
                            <div class="row">
                                <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label" for="single">Aktiflik Durumu</label>
                                            <select name="status" class="form-control ">
                                                <option value="1" @if($item['status'] === 1) selected @endif>Aktif</option>
                                                <option value="0" @if($item['status'] === 0) selected @endif>İnaktif</option>
                                            </select>
                                        </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="single">Hesap Tipi</label>
                                        <select id="role"  name="role" class="form-control ">
                                            <option value="1" @if($item['admin'] === 1) selected @endif>Yönetici</option>
                                            <option value="0" @if($item['admin'] === 0) selected @endif>Üye</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>

                        </div>
                        <br>
                        <div class="form-actions right">
                            <button type="submit" class="btn blue">
                                <i class="fa fa-check"></i> Kaydet
                            </button>
                        </div>
                    </form>
                    <!-- END FORM-->

                        <style>
                            .orderRow td:first-child{
                                display:none;
                            }

                            .orderRow  td:nth-child(3) {
                                text-align: center;
                            }
                            .table th:first-child{
                                display:none;
                            }
                        </style>

                    <br>
                    <br>





                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
@endsection

@section('script')
    <script>


    </script>

@endsection