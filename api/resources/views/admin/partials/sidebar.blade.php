<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item start">
                <a href="{{route('admin.dash')}}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Anasayfa</span>
                </a>
                <!-- Sub menu example
                <ul class="sub-menu">
                    <li class="nav-item start ">
                        <a href="#" class="nav-link ">
                            <i class="icon-bar-chart"></i>
                            <span class="title">Dashboard 1</span>
                        </a>
                    </li>
                </ul>
                Sub Menu End -->
            </li>

            <li class="nav-item start">
                <a href="{{route('admin.news.index')}}" class="nav-link nav-toggle">
                    <i class="glyphicon glyphicon-pencil"></i>
                    <span class="title">Haberler</span>
                </a>
                <ul class="sub-menu">
                    <li><a href="{{route('admin.news.news')}}?type=1">Halden Haberler</a></li>
                    <li><a href="{{route('admin.news.news')}}?type=2">Uzman Bakışı - Haldekiler</a></li>
                    <li><a href="{{route('admin.news.news')}}?type=3">Uzman Bakışı - Nunhems Uzmanları</a></li>
                    <li><a href="{{route('admin.news.news')}}?type=4">Dünya Halleri - Dünyadan Haberler</a></li>
                    <li><a href="{{route('admin.news.news')}}?type=5">Dünya Halleri - Galeri</a></li>
                    <li><a href="{{route('admin.news.news')}}?type=6">Dünya Halleri - Manşet</a></li>
                    <li><a href="{{route('admin.news.news')}}?type=7">Uzman Bakışı - Manşet</a></li>
                    <li><a href="{{route('admin.news.news')}}?type=8">Halden Haberler - Manşet</a></li>
                </ul>
            </li>
            <li class="nav-item start">
                <a href="{{route('admin.markets.index')}}" class="nav-link nav-toggle">
                    <i class="glyphicon glyphicon-th"></i>
                    <span class="title">Haller</span>
                </a>
            </li>
            <li class="nav-item start">
                <a href="{{route('admin.product.index')}}" class="nav-link nav-toggle">
                    <i class="glyphicon glyphicon-shopping-cart"></i>
                    <span class="title">Ürünler</span>
                </a>
            </li>
            <li class="nav-item start">
                <a href="{{route('admin.catalogs.index')}}" class="nav-link nav-toggle">
                    <i class="glyphicon glyphicon-list"></i>
                    <span class="title">Kataloglar</span>
                </a>
            </li>
            <li class="nav-item start">
                <a href="{{route('admin.user.index')}}" class="nav-link nav-toggle">
                    <i class="glyphicon glyphicon-user"></i>
                    <span class="title">Üyeler</span>
                </a>
            </li>
        </ul>
    </div>
</div>
<script>
    $(document).ready(function () {

        if ($("a[href='{{Request::url()}}']").length > 0)
            $("a[href='{{Request::url()}}']").append('<span class="selected"></span><span class="arrow open"></span>').parents('li').addClass('active');
    });


</script>