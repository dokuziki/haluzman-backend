<h3 class="page-title"> </h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{route('admin.dash')}}">Ana Sayfa</a>
            <i class="fa fa-angle-right"></i>
        </li>
        @yield('breadcrumb','')
        <li>
            <span></span>
        </li>
    </ul>
</div>
